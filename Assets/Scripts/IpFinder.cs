using System;
using System.IO;
using System.Net;

public class IpFinder {
	public static String GetPublicIp() {
		HttpWebRequest request = WebRequest.CreateHttp("https://api.ipify.org/");
		HttpWebResponse response = (HttpWebResponse) request.GetResponse();
		String result = new StreamReader(response.GetResponseStream() ?? throw new Exception("IP Fetch failed"))
			.ReadToEnd();
		return result;
	}
}