﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DataModels;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CollectionMenuController : MonoBehaviour {
	private DatabaseManager _databaseManager;
	private List<Card> _cardsCollection;
	private List<GameObject> _spawnedCards;
	public float cardsScale;

	[SerializeField]
	private int rangeCount;

	[SerializeField]
	private int maxSelectedCards;

	private List<Card> _selectedCards;
	public Button playButton;

	private MainMenuManager _mainMenuManager;
	private String _lastBoardMessage;

	private void Awake() {
		_databaseManager = FindObjectOfType<DatabaseManager>();
		_mainMenuManager = FindObjectOfType<MainMenuManager>();
	}

	private void OnEnable() {
		Debug.Log("On Enable");
		User currentUser = _databaseManager.CurrentUser;
		_cardsCollection = new List<Card>(currentUser.CardsCollection);
		_selectedCards = new List<Card>();
		Vector2Int collectionRange = new Vector2Int(0, rangeCount > _cardsCollection.Count ? _cardsCollection.Count : rangeCount);

		SpawnCards(collectionRange);

		_lastBoardMessage = _mainMenuManager.GetBoardMessage();
		playButton.onClick.AddListener(StartMatchmaking);
	}

	private void Update() {
		String message = $"Please select at least one card and up to {maxSelectedCards} cards.\n" +
		                 $"Selected cards count: {_selectedCards.Count}/{maxSelectedCards}";
		_mainMenuManager.SubmitNewBoardMessage(message);
		playButton.interactable = 0 < _selectedCards.Count && _selectedCards.Count <= maxSelectedCards;
	}

	private void OnDisable() {
		DestroyCurrentCards();
		_mainMenuManager.SubmitNewBoardMessage(_lastBoardMessage);
	}

	private void StartMatchmaking() {
		// TODO: Add possibility to cancel matchmaking
		MatchmakingManager.CreateMatchmakingManager(_databaseManager.CurrentUser, _selectedCards);
		SceneManager.LoadScene("GameScene");
	}

	private void SpawnCards(Vector2Int collectionRange) {
		const String TAG = "SpawnCards()";
		if (_cardsCollection == null || _cardsCollection.Count == 0) {
			Debug.LogWarning($"{TAG}: No cards to spawn");
			return;
		}

		DestroyCurrentCards();

		_spawnedCards = _cardsCollection
			.GetRange(collectionRange.x, collectionRange.y)
			.ConvertAll(card => SimpleUICardController.SpawnNewUiCard(card, transform, cardsScale));
	}

	private void DestroyCurrentCards() {
		const String TAG = "DestroyCurrentCards()";
		if (_spawnedCards == null) {
			Debug.LogWarning($"{TAG}: no cards spawned");
			return;
		}
		
		_spawnedCards.ForEach(Destroy);
		_spawnedCards.Clear();
	}

	public void CardSelected(Card card) {
		_selectedCards.Add(card);
		Debug.Log($"Added {card} to selected cards");
	}

	public void CardDeselected(Card card) {
		_selectedCards.Remove(card);
		Debug.Log($"Removed {card} from selected cards");
	}
}