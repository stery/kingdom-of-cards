﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardsGridController : MonoBehaviour {
    public int cardsLimit;

    private GridLayoutGroup _gridLayout;
    private int _cardsCount;
    private List<GameObject> _cardsGameObjects;

    private void Awake() {
        _cardsCount = 0;
        _cardsGameObjects = new List<GameObject>(cardsLimit);
    }

    // Start is called before the first frame update
    void Start() {
        _gridLayout = GetComponent<GridLayoutGroup>();
        // TODO: Count cards, etc.
    }

    /// <summary>
    /// Snaps the given card to the current grid by settings its
    /// parent to this grid (without preserving world position
    /// so it can auto-align), the scale and the layer
    /// </summary>
    /// <param name="cardGameObject">The card to be snapped to this grid</param>
    /// <returns>A Boolean indicating if there was enough room to add the given card</returns>
    public Boolean AddCardToGrid(GameObject cardGameObject) {
        /*
         * TODO:!!!!! NOTIFY THE USER IF IT FAILED TO SOMEHOW PUT IT BACK IN THE HAND
         * SINCE USING NETWORKING REMOVES THE ABILITY TO RETURN TYPES OTHER THAN VOID FROM RPCs AND COMMANDS
         * SO DO STUFF WHEN IT RETURNS FALSE, AND MAYBE REMOVE THE RETURN TYPE
         */
        if (_cardsCount == cardsLimit) {
            Debug.Log("Sorry pal, can't add more cards on this grid: " + this);
            return false;
        }

        cardGameObject.transform.localPosition = Vector3.zero;
        cardGameObject.transform.SetParent(_gridLayout.transform, false);
        // TODO: Check for more transform adjustments
        Vector3 newScale = Vector3.one;
        switch (gameObject.layer) {
            case (int)LayerController.UILayers.Board:
                newScale /= 2;
                break;
            case (int)LayerController.UILayers.Screen:
                break;
            default:
                Debug.LogWarning($"Trying to set the layer of {cardGameObject.name} to {gameObject.layer}");
                break;
        }

        LayerController.SetLayerRecursively(cardGameObject, gameObject.layer);
        Debug.Log($"{cardGameObject.name} layer: {cardGameObject.layer}; {name} layer: {gameObject.layer}");
        cardGameObject.transform.localScale = newScale;
        cardGameObject.GetComponent<CardController>().setPlayed(); // Notify the card so it disables the dragging
        _cardsGameObjects.Add(cardGameObject);
        Debug.Log(String.Format(
            "Added card {0} to this({1}) grid, check should be true: {2}",
            cardGameObject, _gridLayout, cardGameObject.transform.parent == _gridLayout.transform
        ));
        _cardsCount++;
        return true;
    }
}