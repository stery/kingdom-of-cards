﻿using System;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerController : NetworkBehaviour {
    [SerializeField]
    private GameObject myUiGrid;

    public GameObject collectionsMenu;

    [SyncVar]
    public GameObject currentlyDraggedCard;

    [SerializeField]
    private List<PlayerController> playersGrids;

    [SerializeField]
    private List<DummyDeckController> playersDecks;

    public int maxPlayers;
    private List<GameObject> _connectedPlayers;

    public enum GameStatus {
        Awaiting,
        Started,
        Ended
    }

    private GameStatus _gameStatus;

    private void Awake() {
        _connectedPlayers = new List<GameObject>(maxPlayers);

        if (NetworkManager.isHeadless) {
            Debug.Log("In a headless server");
        }

        _gameStatus = GameStatus.Awaiting;
    }

    public void AttachCardToUiGrid(GameObject card) {
        throw new NotImplementedException();
        // TODO
    }

    public void RegisterPlayerGrid(PlayerController playerGrid) {
        if (!playersGrids.Contains(playerGrid)) {
            playersGrids.Add(playerGrid);
        }
    }

    public List<PlayerController> GetPlayersGrids() {
        return playersGrids;
    }

    [Server]
    public void AddConnectedPlayer(GameObject player, String playerId, String playerName) {
        if (!_connectedPlayers.Contains(player)) {
            _connectedPlayers.Add(player);
            FindObjectOfType<ScoreAndTurnManager>().RegisterPlayer(playerId, playerName);
            Debug.Log($"A player ({player}) has connected. Count is now: {_connectedPlayers.Count}");
        }

        if (_connectedPlayers.Count == maxPlayers) {
            // Enable them
//            _connectedPlayers[0].GetComponent<HandGridController>().RpcSetCanStart();
            foreach (GameObject connectedPlayer in _connectedPlayers) {
                PlayerController controller = connectedPlayer.GetComponent<PlayerController>();
                controller.RpcSetCanStart();
            }

            _gameStatus = GameStatus.Started;
        }
    }

    [Client]
    public void ShowDecks() {
        foreach (DummyDeckController deck in playersDecks) {
            deck.gameObject.SetActive(true);
        }
    }

    [Server]
    public void SetGameStatusToEnded() {
        _gameStatus = GameStatus.Ended;
    }

    public GameStatus GetGameStatus => _gameStatus;
}