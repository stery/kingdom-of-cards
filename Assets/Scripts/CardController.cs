﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataModels;
using Firebase.Auth;
using Firebase.Extensions;
using Mirror;
using UnityEngine;

public class CardController : NetworkBehaviour {
    private Boolean _played;
    private Boolean _playable;

    public GameObject frontCanvasGameObject, backCanvasGameObject;
    public Color allyCardHoverColor, enemyCardHoverColor;
    private Boolean _isVisible;

    [SerializeField]
    private GameObject parentGrid; // Owning player
    
    [SerializeField]
    private GameObject myTableGrid;

    [SerializeField]
    private GameObject enemyTableGrid;

    private Boolean _connected;

    public override void OnStartClient() {
        base.OnStartClient();
        _connected = true;
        Debug.Log($"OnStartClient: {name} authority: {hasAuthority} & tag: {tag}");
    }

    private void Start() {
        GameObject table = GameObject.Find("Table");
        foreach (CardsGridController grid in table.GetComponentsInChildren<CardsGridController>()) { // Should only be two
            if (grid.CompareTag("Player")) {
                myTableGrid = grid.gameObject;
                Debug.Log($"myTableGrid should be {grid.name}");
            } else if (grid.CompareTag("Enemy")) {
                enemyTableGrid = grid.gameObject;
                Debug.Log($"enemyTableGrid should be {grid.name}");
            }
        }
        // TODO: Update for network players?
    }

    [Command]
    public void CmdSetCardStats(CardStruct cardStats) {
        Debug.Log($"Received command to set {name} stats to {cardStats}");
        RpcSetCardStats(cardStats);
    }

    [ClientRpc]
    private void RpcSetCardStats(CardStruct cardStats) {
        const String TAG = "RpcSetCardStats()";
        Debug.Log($"{TAG}: Received RPC to set {name} stats to {Card.FromCardStruct(cardStats)}");
        GetComponent<CardStatsController>().cardStats = Card.FromCardStruct(cardStats);
        /*Task.Run(() => true).ContinueWithOnMainThread(task => {
            if (!task.IsCompleted) {
                Debug.LogWarning($"{TAG}: Task failed");
                return;
            }
            Debug.Log($"{TAG}: Trying to set {name} stats to {Card.FromCardStruct(cardStats)} on main thread");
            GetComponent<CardStatsController>().cardStats = Card.FromCardStruct(cardStats);
        });*/
    }

    [Command]
    public void CmdAskGridToAttachThisCard(GameObject parentGrid) {
        Debug.Log($"{name} got its Command invoked for {parentGrid}");
        StartCoroutine(WaitUntilConnectedThenAskGridToConnectThisCard(parentGrid));
    }

    [ClientRpc]
    private void RpcAskGridToAttachThisCard(GameObject parentGrid) {
        Debug.Log($"RpcAskGridToAttachThisCard(): parent auth: {parentGrid.GetComponent<NetworkIdentity>().hasAuthority}, card: {hasAuthority}");
        this.parentGrid = parentGrid;
        this.parentGrid.GetComponent<PlayerController>().AttachCardToHand(gameObject, parentGrid);
    }

    /// <summary>
    /// Disables card movement once played
    /// (once placed on the table)
    /// Also set it visible for all (will be called from an RPC)
    /// </summary>
    public void setPlayed() {
        if (_played) {
            return;
        }

        _played = true;
        DragObjectController dragObjectController = GetComponent<DragObjectController>();
        dragObjectController.enabled = false;
        setVisibility(true);
    }
    
    public void setVisibility(Boolean visible) {
        _isVisible = visible;
        frontCanvasGameObject.SetActive(_isVisible);
        backCanvasGameObject.SetActive(!_isVisible);
    }

    private IEnumerator WaitUntilConnectedThenAskGridToConnectThisCard(GameObject parentGrid) {
        //yield return new WaitUntil(() => _connected); // Probably used before waiting for players to connect
        yield return null;
        Debug.Log($"Waiting done for {name}");
        RpcAskGridToAttachThisCard(parentGrid);
    }
    
    [ClientRpc]
    private void RpcAttachCardToTableGrid(GameObject card) {
        NetworkIdentity cardNetId = card.GetComponentInParent<NetworkIdentity>();
        Debug.Log($"gameManager: {cardNetId.hasAuthority}, {isServer}");
        CardsGridController tableCardsGridController = (cardNetId.hasAuthority ? myTableGrid : enemyTableGrid).GetComponent<CardsGridController>();
        tableCardsGridController.AddCardToGrid(card);
        
        // TODO: Annouce server to update score
        FirebaseUser currentFirebaseUser = FindObjectOfType<LoginManager>().GetCurrentUser();
        Card cardStats = card.GetComponent<CardStatsController>().cardStats;
        Debug.Log("Asking the server to update the score");
        CmdNotifyPlayedCard(currentFirebaseUser.UserId, cardStats);
    }

    [Command]
    private void CmdNotifyPlayedCard(String playerId, Card cardStats) {
        FindObjectOfType<ScoreAndTurnManager>().NotifyPlayedCard(playerId, cardStats);
    }

    [Command] // Workaround for a client to tell the other client what to do...
    public void CmdAttachCardToTableGrid(GameObject card) {
        RpcAttachCardToTableGrid(card);
    }

    public PlayerController GetParentPlayer() {
        return parentGrid.GetComponent<PlayerController>();
    }
}
