﻿using System;
using System.Collections;
using Firebase;
using Firebase.Auth;
using Firebase.Unity.Editor;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoginManager : MonoBehaviour {

    private Boolean _dependenciesFailure;
    private FirebaseApp _app;
    private FirebaseAuth _auth;
    private Boolean _initialised;
    private Boolean _requestDone;

    public TMP_InputField loginInput;
    public TMP_InputField passwordInput;
    public Button loginButton;
    public Button signupButton;
    private MainMenuManager _mainMenuManager;

    private static LoginManager _instance;

    private FirebaseUser _currentUser;

    private void Awake() {
        if (_instance) {
            DestroyImmediate(gameObject);
        } else {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
    }

    private void Start() {
        _mainMenuManager = FindObjectOfType<MainMenuManager>();
        _mainMenuManager.SubmitNewBoardMessage("Connecting to the login server...");
        // TODO: Remove, debug purpose
//        loginInput.text = "c.cristian.sterian@gmail.com";
//        passwordInput.text = "123456";
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available) {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                //   app = Firebase.FirebaseApp.DefaultInstance;
                _app = FirebaseApp.DefaultInstance;
                _app.SetEditorDatabaseUrl("https://kingdom-of-cards-1559820426626.firebaseio.com/");
                _auth = FirebaseAuth.GetAuth(_app);
                // Set a flag here to indicate whether Firebase is ready to use by your app.
                Debug.Log("Holla from the firebase app!");
            } else {
                Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
                // Firebase Unity SDK is not safe to use here.
                _dependenciesFailure = true;
            }

            _initialised = true;
        });
        
        StartCoroutine(ContinueStartup());
    }
    
    private void SignupUser() {
        String email = loginInput.text;
        String password = passwordInput.text;
        _requestDone = false;
        StartCoroutine(DisplayPendingRequestMessage());
        _auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            _requestDone = true;
            String boardMessage;
            if (task.IsCanceled) {
                boardMessage = "CreateUserWithEmailAndPasswordAsync was canceled.";
                Debug.LogError(boardMessage);
                _mainMenuManager.SubmitNewBoardMessage(boardMessage);
                return;
            }
            if (task.IsFaulted) {
                boardMessage = "CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception;
//                                task.Exception.InnerExceptions
//                                    .Where(ex => ex is FirebaseException)
//                                    .Select(ex => ex.ToString())
//                                    .Aggregate((ex1, ex2) => ex1 + "\n" + ex2);
                Debug.LogError(boardMessage);
                _mainMenuManager.SubmitNewBoardMessage(boardMessage);
                return;
            }

            // Firebase user has been created.
            _currentUser = task.Result;
            boardMessage = $"Firebase user created successfully: {_currentUser.DisplayName} ({_currentUser.UserId})";
            Debug.Log(boardMessage);
            _mainMenuManager.SubmitNewBoardMessage(boardMessage);
        });
    }

    private void LoginUser() {
        String email = loginInput.text;
        String password = passwordInput.text;
        _requestDone = false;
        StartCoroutine(DisplayPendingRequestMessage());
        _auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            _requestDone = true;
            String boardMessage;
            if (task.IsCanceled) {
                boardMessage = "SignInWithEmailAndPasswordAsync was canceled.";
                Debug.LogError(boardMessage);
                _mainMenuManager.SubmitNewBoardMessage(boardMessage);
                return;
            }
            if (task.IsFaulted) {
                boardMessage = "SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception;
//                                task.Exception.InnerExceptions
//                                    .Where(ex => ex is FirebaseException)
//                                    .Select(ex => ex.ToString())
//                                    .Aggregate((ex1, ex2) => ex1 + "\n" + ex2);
                Debug.LogError(boardMessage);
                _mainMenuManager.SubmitNewBoardMessage(boardMessage);
                return;
            }

            _currentUser = task.Result;
            boardMessage = $"User signed in successfully: {_currentUser.DisplayName} ({_currentUser.UserId})";
            Debug.Log(boardMessage);
            _mainMenuManager.SubmitNewBoardMessage(boardMessage);
        });
    }

    private Boolean SanityCheck() {
        if (_dependenciesFailure) {
            //SetMessageBoard(_latestBoardMessage);
            return false;
        }
        return true;
    }

    public IEnumerator DisplayPendingRequestMessage() {
        DisableButtons();
        _mainMenuManager.SubmitNewBoardMessage("Waiting for response from the server...");
        yield return new WaitUntil(() => _requestDone);
        if (_currentUser == null) {
            // There was an error on login or sign up, the user is not authenticated yet
            EnableButtons();
        }
    }
    
    private void DisableButtons() {
        loginButton.interactable = false;
        signupButton.interactable = false;
    }

    private void EnableButtons() {
        loginButton.interactable = true;
        signupButton.interactable = true;
    }

    private IEnumerator ContinueStartup() {
        yield return new WaitUntil(() => _initialised);
        Debug.Log("Passed the firebase init stuff");

        if (SanityCheck()) {
            Debug.Log("Start Sanity check passed");
//            _auth = FirebaseAuth.GetAuth(_app); // This is crashing Unity
//            _auth = FirebaseAuth.DefaultInstance;
            _mainMenuManager.SubmitNewBoardMessage("Connected");
            loginButton.interactable = true;
            signupButton.interactable = true;
            loginButton.onClick.AddListener(LoginUser);
            signupButton.onClick.AddListener(SignupUser);
        } else {
            Debug.LogWarning("Start Sanity check failed");
        }
    }

    public Boolean GetInitialisedStatus() {
        return _initialised;
    }

    public FirebaseUser GetCurrentUser() {
        return _currentUser;
    }

}
