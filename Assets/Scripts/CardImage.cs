﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class CardImage : MonoBehaviour {
    public SpriteRenderer frontSpriteRenderer;
    public Image frontImage;

    private Sprite _sprite;
    private Boolean _shouldUpdateSprite;

    private void Update() {
        if (_shouldUpdateSprite) {
            if (frontSpriteRenderer == null) {
                frontImage.sprite = _sprite;
            } else {
                frontSpriteRenderer.sprite = _sprite;
            }
            _shouldUpdateSprite = false;
        }
    }

    public void ChangeSpriteWithResourceName(String spriteName) {
        _sprite = Resources.Load<Sprite>($"Sprites/{spriteName}");
        _shouldUpdateSprite = true;
    }

}
