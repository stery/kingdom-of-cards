﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using DataModels;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class MapCardController : MonoBehaviour {
    private Vector3 _initialPos;

    public float floatingSpeed = 2f;
    public float floatingAmplitude = 1f;
    public float rotationSpeed = 50f;

    [SerializeField]
    private GameObject faces; // Will float the canvases, not this GameObject, so the circle doesn't move

    enum CardStatus {
        AVAILABLE,
        CLAIMED
    }
    
    private Boolean _clickable;
    private RangeCircle _rangeCircle;
    public Color availableCardColor;
    public Color claimedCardColor;
    private int _claimerId;
    private Time _timeClaimed; // Should be used when implementing PvP, represents the time at which the card was claimed
    private float _timeAvailable; // Should be used when implementing PvP
    private CardStatus _cardStatus;
//    private GameObject _player;

    public LocationReward locationReward;

    private void Start() {
        _rangeCircle = GetComponent<RangeCircle>();
        _rangeCircle.color = availableCardColor;
        
//        _player = GameObject.Find("PlayerTarget");
        
        Vector3 newPos = transform.localPosition;
        newPos.y = 5.0f;
        faces.transform.localPosition = newPos;
        _initialPos = faces.transform.localPosition;
        
        Debug.Log($"{name} face location: {faces.transform.localPosition}, while parent is {transform.localPosition}");
        faces.transform.localPosition = Vector3.zero;
    }

    private void Update() {
        faces.transform.Rotate(new Vector3(0, Time.deltaTime * rotationSpeed));
        
        double yPos = Time.fixedTime * floatingSpeed;
        yPos = Math.Sin(yPos) * floatingAmplitude + _initialPos.y;
        Vector3 newPos = faces.transform.localPosition;
        newPos.y = (float)yPos;
        faces.transform.localPosition = newPos;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            Debug.Log($"Player collided with {name}");
            _clickable = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            _clickable = false;
            Debug.Log($"Player no longer colliding with {name}");
        }
    }

    private void OnMouseUp() {
        // TODO: Check if should disable the outline
        Debug.Log($"Clicking {name}");
        if (_clickable) {
            //TODO: Stuff, claim, show info
            if (_cardStatus == CardStatus.AVAILABLE) {
                ClaimCard();
            }
        }
    }

    private void ClaimCard() {
        _rangeCircle.color = claimedCardColor;
        _rangeCircle.DrawPoints();
        _cardStatus = CardStatus.CLAIMED;
        
        /*
         * TODO: For now, it will just collect the card,
         * but in the future it should mark the card as claimed,
         * set timer, wait for opponents, add it to the user's
         * inventory only if he wins or the timer expires
         *
         * TODO: Check if it still exists (all references are valid,
         * even with a listener attached in the MapManager)
         */

        DatabaseManager databaseManager = FindObjectOfType<DatabaseManager>();
        CardStatsController cardStatsController = GetComponent<CardStatsController>();
        databaseManager.RemoveLocationReward(locationReward, () => {
            databaseManager.AddCardToUserCollection(cardStatsController.cardStats, () => {
                // TODO: Also make a check if the card is still available when clicked, maybe
                // TODO: Or use a Firebase listener on locations, which would solve both
                //Destroy(gameObject, 3); // The MapManager takes care of it when signaled of the removal
            });
        });
    }
    
}
