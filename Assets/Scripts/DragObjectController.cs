﻿using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class DragObjectController : MonoBehaviour {
    private Transform _initialTransform;
    private int _previousLayer;

    private GameManagerController _gameManagerController;
    private ScoreAndTurnManager _scoreAndTurnManager;
    private NetworkIdentity _networkIdentity;
    private Boolean _movable = true;
    private Boolean _ownerTurn = false;
    private Boolean _owned;
    private Boolean _dragging;

//    private Vector3 _screenPoint;
//    private Vector3 _offset;
    // Start is called before the first frame update
    void Start() {
        _initialTransform = transform;
        _gameManagerController = FindObjectOfType<GameManagerController>();
        _scoreAndTurnManager = FindObjectOfType<ScoreAndTurnManager>();
        _networkIdentity = GetComponent<NetworkIdentity>();
    }

    private void Update() {
        if (NetworkManager.isHeadless) {
            return;
        }
        PlayerController parentPlayerController = GetComponent<CardController>().GetParentPlayer();
        _ownerTurn = _scoreAndTurnManager.currentPlayer == parentPlayerController.PlayerId;
//        _ownerTurn = true; // Debug purpose; TODO: Remove
    }

    private void OnMouseDown() {
//        _screenPoint = Camera.main.WorldToScreenPoint(transform.position);
//        _offset = transform.position - Camera.main.ScreenToWorldPoint(
//                      new Vector3(Input.mousePosition.x, Input.mousePosition.y, _screenPoint.z)
//                  );
        _owned = _networkIdentity.hasAuthority;
        Debug.Log($"OnMouseDown(): movable: {_movable}, owned: {_owned}, owner's turn: {_ownerTurn}; Current player: {_scoreAndTurnManager.currentPlayer}");
        if (_movable && _owned && _ownerTurn) {
            _previousLayer = gameObject.layer;
            LayerController.SetLayerRecursively(gameObject, (int) LayerController.UILayers.Board);
        }
    }

    private void OnMouseDrag() {
        _owned = _networkIdentity.hasAuthority;
        if (_movable && _owned && _ownerTurn) {
            Debug.Log($"Dragging {name}, {enabled}, {_movable}");
            Vector3 screenPoint = Camera.main.WorldToScreenPoint(transform.position);
            Vector3 cursorScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
            Vector3 cursorWorldPos = Camera.main.ScreenToWorldPoint(cursorScreenPoint);
            transform.position = cursorWorldPos;

            _gameManagerController.currentlyDraggedCard = gameObject;
            _dragging = true;
        }
    }

    private void OnMouseUp() {
        /*
         * TODO: Find a way to check if the current card was just clicked,
         * not dragged, so maybe the player only wanted to see the stats
         * of the card (because there is no "onMouseEnter" for mobile, they
         * have to tap it to see it grow and highlighted
         * TODO: Check if the _dragging fixes it
         */
        _owned = _networkIdentity.hasAuthority;
        if (_movable && _owned && _ownerTurn) {
            LayerController.SetLayerRecursively(gameObject, _previousLayer);
            // Attach card to board, if room available
            // TODO: Check if it left the hand area?
            NetworkIdentity networkIdentity = gameObject.GetComponent<NetworkIdentity>();
            CardController cardController = gameObject.GetComponent<CardController>();
            Debug.Log($"Trying to attach {gameObject} to table, auth: {networkIdentity.hasAuthority}, mgrAuth: {_gameManagerController.hasAuthority}");
            cardController.CmdAttachCardToTableGrid(gameObject);
            /*if (!_gameManagerController.CmdAttachCardToTableGrid(gameObject)) {
                // Could not attach it, put it back in the hand?
                _gameManagerController.AttachCardToUiGrid(gameObject);
                // TODO: Check if behaviours as expected
            }*/

            if (_gameManagerController.currentlyDraggedCard == gameObject) {
                _gameManagerController.currentlyDraggedCard = null;
            }

            if (networkIdentity.hasAuthority) {
                _gameManagerController.GetPlayersGrids()
                    .Find(player => player.isLocalPlayer)
                    .RemoveOwnedCardFromList(gameObject);
            }
        }
        _dragging = false;
    }

    public void OnDisable() {
//        Debug.Log($"Someone disabled me on {name}");
        _movable = false;
    }
}
