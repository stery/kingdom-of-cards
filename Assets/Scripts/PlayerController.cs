﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using DataModels;
using Mirror;
using UnityEngine;

public class PlayerController : NetworkBehaviour
{
    // Basically the player controller for now

    public GameObject UiHandGridPlaceholder;
    public GameObject TableEnemyHandGridPlaceholder;
    public GameObject cardPrefab;

    private GameManagerController _gameManagerController;
    private Boolean _canStart;
    private String _playerId;
    private String _playerName;

    private List<GameObject> _ownedCardsInHand;
    /*public override void OnStartAuthority() {
        NetworkIdentity nid = GetComponent<NetworkIdentity>();
        Debug.Log($"OnStartClient: {name}, isLocalPlayer: {nid.isLocalPlayer}, hasAuth: {nid.hasAuthority}");
        if (isLocalPlayer) {
            CmdRegisterPlayer(gameObject);
        }
    }*/

    private void Start() {
        UiHandGridPlaceholder = GameObject.Find("HandGridPlaceholder");
        TableEnemyHandGridPlaceholder = GameObject.Find("EnemyHandGridPlaceholder");
        _gameManagerController = FindObjectOfType<GameManagerController>();
        transform.localPosition = Vector3.zero;
        _playerId = FindObjectOfType<LoginManager>().GetCurrentUser().UserId;
        _playerName = FindObjectOfType<DatabaseManager>().CurrentUser.Username;
        Debug.Log($"OnStart: {name}, isLocalPlayer: {isLocalPlayer}, hasAuth: {hasAuthority}");
        if (isLocalPlayer) {
            // Easier tracking in the Inspector
            if (isServer) {
                name += "(Host)";
            } else {
                name += "(Client)";
            }

            tag = "Player";
            CmdRegisterPlayer(gameObject, _playerId, _playerName);
            // Set the parent and layer to the UI one and show all the cards
            transform.SetParent(UiHandGridPlaceholder.transform, false);
            LayerController.SetLayerRecursively(gameObject, (int)LayerController.UILayers.Screen);
            /*foreach (CardController card in GetComponentsInChildren<CardController>())
            {
                card.setVisibility(true);
            }*/
            _ownedCardsInHand = new List<GameObject>();
        } else {
            tag = "Enemy";
            // Enemy's cards, place then on the board
            transform.SetParent(TableEnemyHandGridPlaceholder.transform, false);
            LayerController.SetLayerRecursively(gameObject, (int)LayerController.UILayers.Board);
            // ??? TODO: Check if anything else to do
        }
        transform.localRotation = Quaternion.identity;

        _gameManagerController.RegisterPlayerGrid(this);

        if (isLocalPlayer) {
            StartCoroutine(WaitUntilCanStart());
        }
    }

    /// <summary>
    /// Instantiates a new card using the cardPrefab and
    /// spawns the card with the parent's (a hand grid a.k.a. player)
    /// authority, then calls CmdAskGridToAttachThisCard on that card
    /// so it synchronizes the parent grid for each player.
    /// 
    /// So, "simply" put, the flow when adding a card is as follows:
    /// Someone (for now only players (hand grids)) calls this ->
    /// -> instantiate & spawn card w/ authority -> run command
    /// on card to signal the parent grid to attach it -> parent
    /// grid attaches the card.
    /// </summary>
    /// <param name="parent">
    /// The parent GameObject this card will be attached to,
    /// which should be a hand grid (a player)
    /// </param>
    /// <param name="cardStats">
    /// The card's stats with which should be initialised
    /// </param>
    [Command]
    private void CmdSpawnCard(GameObject parent, CardStruct cardStats) {
        GameObject card = Instantiate(cardPrefab);
        NetworkServer.SpawnWithClientAuthority(card, parent);
        Debug.Log($"{name} spawned {card}, will try to attach to {parent}");
        CardController cardController = card.GetComponent<CardController>();
        cardController.CmdAskGridToAttachThisCard(parent);
        cardController.CmdSetCardStats(cardStats); // TODO: Check if set properly
//        AttachCardToHand(card);
    }

    [Command]
    public void CmdAttachCardToHand(GameObject card, GameObject parent) {
        //AttachCardToHand(card, parent);
    }
    
    public void AttachCardToHand(GameObject card, GameObject parent) {
        NetworkIdentity cardNetworkIdentity = card.GetComponent<NetworkIdentity>();
        CardController cardController = card.GetComponent<CardController>();
        card.transform.localPosition = Vector3.zero;
        card.transform.localScale = Vector3.one;
        Debug.Log($"{name} (with authority {hasAuthority} trying to add {card} with authority: {cardNetworkIdentity.hasAuthority}. I am local: {isLocalPlayer}");
        card.tag = parent.tag;
        card.GetComponent<Outline>().OutlineColor = card.CompareTag("Player")
            ? cardController.allyCardHoverColor
            : cardController.enemyCardHoverColor;
        card.transform.SetParent(parent.transform, false);
        if (cardNetworkIdentity.hasAuthority) {
            // Our card
            LayerController.SetLayerRecursively(card, (int)LayerController.UILayers.Screen);
            cardController.setVisibility(true);
            _ownedCardsInHand.Add(card);
        } else {
            // Not our card
            //card.transform.SetParent(TableEnemyHandGridPlaceholder.transform, false);
            LayerController.SetLayerRecursively(card, (int)LayerController.UILayers.Board);
            cardController.setVisibility(false);
        }
    }

    // This should contain all the logic for when the players can start playingci 
    private IEnumerator WaitUntilCanStart() {
        const String TAG = "WaitUntilCanStart()";
        yield return new WaitUntil(() => _canStart);
        // Make decks visible
        _gameManagerController.ShowDecks();
        // TODO: Spawn cards
        Debug.Log($"{TAG}: {name} can now start, trying to spawn a card, authority: {hasAuthority}");
//        CmdSpawnCard(gameObject);
        
        //TryNotifyInitialised(); // FOR SOME REASON, IT WON'T RUN THE COMMAND...
        MatchmakingManager.GetInstance().startingCards.ForEach(card => CmdSpawnCard(gameObject, card.ToCardStruct()));

        CmdNotifyInitialised(_playerId);
    }

    [Command]
    private void CmdRegisterPlayer(GameObject player, String playerId, String playerName) {
        _gameManagerController.AddConnectedPlayer(player, playerId, playerName);
        Debug.Log($"Testing general command behaviour... is this {gameObject} equal to player {player.gameObject}? {gameObject == player.gameObject}");
    }

    [ClientRpc]
    public void RpcSetCanStart() {
        Debug.Log($"{name} can now start.");
        _canStart = true;
    }

    public void RemoveOwnedCardFromList(GameObject card) {
        _ownedCardsInHand.Remove(card);
    }

    public String PlayerName => _playerName;
    public String PlayerId => _playerId;

    [Command]
    public void CmdEndTurn() {
        FindObjectOfType<ScoreAndTurnManager>().EndTurn();
    }

    [Command]
    private void CmdNotifyInitialised(String playerId) {
        const String TAG = "CmdNotifyInitialised()";
        Debug.Log($"{TAG}: Notified by {playerId}");
        FindObjectOfType<ScoreAndTurnManager>().NotifyPlayerInitialised(playerId);
    }
}
