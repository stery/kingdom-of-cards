﻿using System;
using System.Collections;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {
    private LoginManager _loginManager;
    private DatabaseManager _databaseManager;

    public GameObject loginMenu;
    public GameObject mainMenu;
    public TextMeshProUGUI messagesBoard;
    private String _latestBoardMessage;
    private Boolean _shouldUpdateBoard;

    public Button mapButton;
    public Button cardsButton; // leading to matchmakingMenu now; TODO: rename

    public GameObject matchmakingMenu;

    private Thread _mainThread;

    public Thread MainThread => _mainThread;

    private void Awake() {
        _mainThread = Thread.CurrentThread;
    }

    private void Start() {
        _loginManager = FindObjectOfType<LoginManager>();
        Boolean loggedIn = _loginManager.GetCurrentUser() != null;
        
        mapButton.onClick.AddListener(MapButtonHandler);
        cardsButton.onClick.AddListener(CollectionsButtonHandler);
        
        if (loggedIn) {
            DisplayMainMenu();
        } else {
            DisplayLoginMenu();
        }
    }

    private void Update() {
        if (Input.GetKeyUp(KeyCode.Escape)) {
            // Pressed Back on phone or Escape on PC
            if (matchmakingMenu.activeSelf) {
                matchmakingMenu.SetActive(false);
                mainMenu.SetActive(true);
                return;
            }
            
            Application.Quit();
            return;
        }
        
        if (_shouldUpdateBoard) {
            SetMessageBoard(_latestBoardMessage);
            _shouldUpdateBoard = false;
        }
    }
    
    public void SubmitNewBoardMessage(String message) {
        _latestBoardMessage = message;
        _shouldUpdateBoard = true;
    }

    private void SetMessageBoard(String message) {
        messagesBoard.text = message;
    }

    public void DisplayMainMenu() {
        loginMenu.SetActive(false);
        mainMenu.SetActive(true);
        mapButton.interactable = true;
        cardsButton.interactable = true;
        Debug.Log($"MainMenu DisplayMainMenu(): current user: {_loginManager.GetCurrentUser().Email}");
    }

    public void DisplayLoginMenu() {
        loginMenu.SetActive(true);
        mainMenu.SetActive(false);
        mapButton.interactable = false;
        cardsButton.interactable = false;
    }

    private void MapButtonHandler() {
        SceneManager.LoadScene("MapScene");
    }

    private void CollectionsButtonHandler() {
        mainMenu.SetActive(false);
        matchmakingMenu.SetActive(true);
    }

    public String GetBoardMessage() {
        return _latestBoardMessage;
    }
}
