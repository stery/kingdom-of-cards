using System;
using System.Collections.Generic;

namespace DataModels {
	[Serializable]
	public class Deck {
		public string DeckName;
		public List<Card> Cards;

		public Deck(string deckName, List<Card> cards) {
			DeckName = deckName;
			Cards = new List<Card>(cards);
		}

		public Dictionary<string, object> ToSimpleDictionary() {
			Dictionary<string, object> result = new Dictionary<string, object> {["DeckName"] = DeckName};
			List<object> cards = new List<object>();
			foreach (Card card in Cards) {
				cards.Add(card.ToSimpleDictionary());
			}

			result["Cards"] = cards;
			return result;
		}

		public static Deck RebuildFromDictionary(Dictionary<string, object> dictionary) {
			if (dictionary == null) {
				return null;
			}

			object deckName;
			dictionary.TryGetValue("DeckName", out deckName);
			List<Card> cards;
			object cardsList;
			dictionary.TryGetValue("Cards", out cardsList);
			cards = Card.RebuildListFromListOfDictionaries(cardsList as List<object>);
			return new Deck(deckName as string, cards);
		}

		public static List<Deck> RebuildListFromListOfDictionaries(List<object> list) {
			if (list == null) {
				return null;
			}

			List<Deck> result = new List<Deck>();
			foreach (var childDeck in list) {
				if (!(childDeck is Dictionary<string, object>)) {
					continue;
				}

				Dictionary<string, object> dictionaryDeck = (Dictionary<string, object>) childDeck;
				Deck deck = RebuildFromDictionary(dictionaryDeck);
				if (deck != null) {
					result.Add(deck);
				}
			}

			return result;
		}
	}
}