﻿using System;
using System.Collections.Generic;
using Firebase.Database;
using UnityEngine;

namespace DataModels {
	[Serializable]
	public class Card {
		public String CardName;
		public int Attack;
		public int Defense;

		public Card() {
			// Mirror needs this
		}

		public Card(string cardName, int attack, int defense) {
			CardName = cardName;
			Attack = attack;
			Defense = defense;
		}

		public override string ToString() {
			return JsonUtility.ToJson(this);
		}

		public CardStruct ToCardStruct() {
			Card thisCard = this;
			CardStruct cardStruct = new CardStruct
				{CardName = thisCard.CardName, Attack = thisCard.Attack, Defense = thisCard.Defense};
			return cardStruct;
		}

		public static Card FromCardStruct(CardStruct cardStruct) {
			return new Card(cardStruct.CardName, cardStruct.Attack, cardStruct.Defense);
		}

		public Dictionary<string, object> ToSimpleDictionary() {
			Dictionary<string, object> result = new Dictionary<string, object>
			{
				["CardName"] = CardName,
				["Attack"] = Attack,
				["Defense"] = Defense
			};
			return result;
		}

		public static Card RebuildFromDictionary(Dictionary<string, object> dictionary) {
			if (dictionary == null) {
				return null;
			}

			object cardName;
			object attack;
			object defense;
			dictionary.TryGetValue("CardName", out cardName);
			dictionary.TryGetValue("Attack", out attack);
			dictionary.TryGetValue("Defense", out defense);
			string stringCardName = cardName as string;
			int intAttack = (int) (attack ?? 0);
			int intDefense = (int) (defense ?? 0);
			return new Card(stringCardName, intAttack, intDefense);
		}

		public static List<Card> RebuildListFromListOfDictionaries(List<object> list) {
			if (list == null) {
				return null;
			}

			List<Card> result = new List<Card>();
			foreach (var childCard in list) {
				if (!(childCard is Dictionary<string, object>)) {
					continue;
				}

				Dictionary<string, object> dictionaryChild = (Dictionary<string, object>) childCard;
				Card card = RebuildFromDictionary(dictionaryChild);
				if (card != null) {
					result.Add(card);
				}
			}

			return result;
		}
	}

	[Serializable]
	public struct CardStruct {
		public string CardName;
		public int Attack;
		public int Defense;
	}
}