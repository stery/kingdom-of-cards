﻿using System;
using System.Collections.Generic;
using System.Linq;
using Firebase.Database;
using UnityEngine;

namespace DataModels {
	[Serializable]
	public class User {
//		public String UserId;
		public String Username;
		public List<Deck> Decks;
		public List<Card> CardsCollection;

//		public User(String userId, String username, List<Deck> decks) {
//			UserId = userId;
//			Username = username;
//			Decks = new List<Deck>(decks);
//		}

		public User(String username, List<Deck> decks, List<Card> cardsCollection) {
			Username = username;
			Decks = new List<Deck>(decks);
			CardsCollection = new List<Card>(cardsCollection);
		}

		public static User CreateStarterUser(String username) {
			List<Card> cardsCollection = new Card[]
			{
				new Card("The Knight", 2, 3),
				new Card("The Horsewoman", 5, 2) 
			}.ToList();
			List<Deck> decks = new Deck[]
			{
				new Deck("First Deck", cardsCollection) 
			}.ToList();
			User user = new User(username, decks, cardsCollection);
			return user;
		}

		public Dictionary<string, object> ToSimpleDictionary() {
			Dictionary<string, object> result = new Dictionary<string, object> {["Username"] = Username};
			List<object> decks = new List<object>();
			foreach (Deck deck in Decks) {
				decks.Add(deck.ToSimpleDictionary());
			}

			List<object> cards = new List<object>();
			foreach (Card card in CardsCollection) {
				cards.Add(card.ToSimpleDictionary());
			}

			result["Decks"] = decks;
			result["CardsCollection"] = cards;
			return result;
		}

		public static User RebuildFromMutableData(MutableData mutableData) {
			if (!(mutableData.Value is Dictionary<string, object>)) {
				return null;
			}

			Dictionary<string, object> dictionaryData = (Dictionary<string, object>) mutableData.Value;
			object username;
			object decksList;
			object cardsList;
			dictionaryData.TryGetValue("Username", out username);
			dictionaryData.TryGetValue("Decks", out decksList);
			dictionaryData.TryGetValue("CardsCollection", out cardsList);
			List<Deck> decks = Deck.RebuildListFromListOfDictionaries(decksList as List<object>);
			List<Card> cardsCollection = Card.RebuildListFromListOfDictionaries(cardsList as List<object>);
			return new User(username as string, decks, cardsCollection);
		}

		public override string ToString() {
			return JsonUtility.ToJson(this);
		}
	}
}