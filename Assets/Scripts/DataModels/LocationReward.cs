using System;
using System.Collections.Generic;
using Firebase.Database;
using UnityEngine;

namespace DataModels {
	[Serializable]
	public class LocationReward {
		public Card Card;
		public double Latitude;
		public double Longitude;

		public LocationReward(Card card, double latitude, double longitude) {
			Card = card;
			Latitude = latitude;
			Longitude = longitude;
		}

		public override string ToString() {
			return JsonUtility.ToJson(this);
		}

		protected bool Equals(LocationReward other) {
			return ToString().Equals(other.ToString());
		}

		public override bool Equals(object obj) {
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((LocationReward) obj);
		}

		public override int GetHashCode() {
			return ToString().GetHashCode();
		}
	}

	[Serializable]
	public class Locations {
		public List<LocationReward> locations;

		public static List<LocationReward> RebuildFromMutableData(MutableData mutableData) {
			if (!(mutableData.Value is List<object>)) {
				return null;
			}

			List<LocationReward> result = new List<LocationReward>();
			foreach (var child in (List<object>) mutableData.Value) {
				if (!(child is Dictionary<string, object>)) {
					continue;
				}

				Dictionary<string, object> dictChild = child as Dictionary<string, object>;
				object card;
				object latitude;
				object longitude;
				dictChild.TryGetValue("Card", out card);
				dictChild.TryGetValue("Latitude", out latitude);
				dictChild.TryGetValue("Longitude", out longitude);
				LocationReward locationReward = new LocationReward(
					Card.RebuildFromDictionary(card as Dictionary<string, object>),
					Double.Parse(latitude as string ?? "0"), Double.Parse(longitude as string ?? "0")
				);
				result.Add(locationReward);
			}

			return result;
		}
	}
}