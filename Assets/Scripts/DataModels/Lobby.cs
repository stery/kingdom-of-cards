using System;
using System.Collections.Generic;
using UnityEngine;

namespace DataModels {
	[Serializable]
	public class Lobby {
		public string IpAddress;

		public string Port;

//		public Boolean Available; // Is there room for a client, beside the host?
		public string ConnectedPlayers;

		public Lobby(string ipAddress, string port, string connectedPlayers) {
			IpAddress = ipAddress;
			Port = port;
			ConnectedPlayers = connectedPlayers;
		}

		public Dictionary<string, object> ToSimpleDictionary() {
			Dictionary<string, object> result = new Dictionary<string, object>
				{["IpAddress"] = IpAddress, ["Port"] = Port, ["ConnectedPlayers"] = ConnectedPlayers};
			return result;
		}

		public void CopyLobbyToDictionary(Dictionary<string, object> dictionary) {
			dictionary["IpAddress"] = IpAddress;
			dictionary["Port"] = Port;
			dictionary["ConnectedPlayers"] = ConnectedPlayers;
		}

		public static Lobby FromSimpleDictionary(Dictionary<string, object> dictionary) {
			Lobby lobby = null;
			try {
				string ip = dictionary["IpAddress"] as string;
				string port = dictionary["Port"] as string;
				string players = dictionary["ConnectedPlayers"] as string;
				lobby = new Lobby(ip, port, players);
			} catch (Exception e) {
				Debug.LogWarning($"Could not convert to lobby from simple dictionary: {e.Message}");
			}

			return lobby;
		}

		public override string ToString() {
			return JsonUtility.ToJson(this);
		}
	}
}