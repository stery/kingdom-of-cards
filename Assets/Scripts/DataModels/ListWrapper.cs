using System;
using System.Collections.Generic;

namespace DataModels {
	[Serializable]
	public class ListWrapper<T> {
		public List<T> list;
	}
}