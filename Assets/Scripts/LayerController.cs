﻿using System;
using UnityEngine;

public class LayerController : MonoBehaviour {
    public enum UILayers {
        Board = 5,
        Screen = 8
    }

    private void FlipLayer() {
        int targetLayer = gameObject.layer == (int)UILayers.Board ? (int)UILayers.Screen : (int)UILayers.Board;
        SetLayerRecursively(gameObject, targetLayer);
    }

    public static void SetLayerRecursively(GameObject gameObject, int layer) {
        if (gameObject == null) {
            return;
        }

        gameObject.layer = layer;
        foreach (Transform t in gameObject.transform) {
            if (t == null) {
                continue;
            }
            SetLayerRecursively(t.gameObject, layer);
        }
    }
}