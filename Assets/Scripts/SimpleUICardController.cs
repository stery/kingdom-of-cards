﻿using System;
using System.Collections;
using System.Threading;
using DataModels;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class SimpleUICardController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
	public TextMeshProUGUI attackText;
	public TextMeshProUGUI defenseText;

	public GameObject outline;
	private Card _card;
	private Boolean _isInitialised;
	private Boolean _isSelected;

	private void Start() {
		StartCoroutine(AwaitInitialisation());
	}

	private IEnumerator AwaitInitialisation() {
		yield return new WaitUntil(() => _isInitialised);
		attackText.text = _card.Attack.ToString();
		defenseText.text = _card.Defense.ToString();
	}

	private void SetCard(Card card) {
		_card = card;
		GetComponent<CardImage>().ChangeSpriteWithResourceName(card.CardName);
		_isInitialised = true;
	}

	public Card getCard() {
		return _card;
	}

	public static GameObject SpawnNewUiCard(Card card, Transform parent, float scale = 1f) {
		const String TAG = "SpawnNewUiCard()";
		// Ensure this is called from the main thread; I should add this check in other places too
		Thread mainThread = FindObjectOfType<MainMenuManager>().MainThread;
		if (!Thread.CurrentThread.Equals(mainThread)) {
			Debug.LogWarning($"{TAG}: This thread ({Thread.CurrentThread.Name}) is not main thread ({mainThread.Name}), aborting");
			return null;
		}
		
		GameObject cardPrefab = Resources.Load<GameObject>("Prefabs/SimpleUICard");
		GameObject newCard = Instantiate(cardPrefab, Vector3.zero, Quaternion.identity, parent);
		newCard.transform.localScale = Vector3.one * scale;
		newCard.GetComponent<SimpleUICardController>().SetCard(card);
		return newCard;
	}

	public void OnPointerEnter(PointerEventData eventData) {
		Debug.Log($"Mouse entered {name}");
		outline.SetActive(true);
	}

	public void OnPointerExit(PointerEventData eventData) {
		Debug.Log($"Mouse exited {name}");
		if (!_isSelected) {
			outline.SetActive(false);
		}
	}

	public void OnPointerClick(PointerEventData eventData) {
		Debug.Log($"Mouse clicked {name}");
		_isSelected = !_isSelected;

		CollectionMenuController collectionMenuController = FindObjectOfType<CollectionMenuController>();
		if (_isSelected) {
			collectionMenuController.CardSelected(_card);
		} else {
			collectionMenuController.CardDeselected(_card);
		}
	}
}