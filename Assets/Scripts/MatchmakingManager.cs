﻿using System;
using System.Collections;
using System.Collections.Generic;
using DataModels;
using Firebase.Auth;
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MatchmakingManager : MonoBehaviour {
	/*
	 * TODO: Add matchmaking details like is host/client,
	 * host IP and port, etc
	 */
	private static GameObject _instance;
	public User currentUser;
	public List<Card> startingCards;
	private Boolean _isConnected, _isHost;

	// How often and how many times to retry connecting to a lobby (maybe the found one was removed in the meantime)
	public int retriesInterval = 3, maxRetriesCount = 5, currentRetriesCount;

	private void Awake() {
		if (_instance) {
			DestroyImmediate(gameObject); // It shouldn't happen because this should be created only with Create method, which handles this case
		} else {
			DontDestroyOnLoad(gameObject);
		}
	}

	private void Start() {
		StartCoroutine(StartMatchmaking());
	}

	private void Update() {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			// Pressed Back on phone or Escape on PC
			ReturnToMainMenu();
		}
	}

	public static void CreateMatchmakingManager(User user, List<Card> selectedCards) {
		if (_instance == null) {
			GameObject prefab = Resources.Load<GameObject>("Prefabs/MatchmakingManager");
			_instance = Instantiate(prefab, Vector3.zero, Quaternion.identity);
			MatchmakingManager matchmakingManager = _instance.GetComponent<MatchmakingManager>();
			matchmakingManager.currentUser = user;
			matchmakingManager.startingCards = new List<Card>(selectedCards);
			matchmakingManager.currentRetriesCount = 0;
			Debug.Log("Created Matchmaking Manager");
		} else {
			Debug.LogWarning("Matchmaking Manager already exists!");
		}
	}

	public static MatchmakingManager GetInstance() {
		if (_instance == null) {
			Debug.LogWarning("No instances running!");
			return null;
		}

		return _instance.GetComponent<MatchmakingManager>();
	}

	public void ReturnToMainMenu() {
		Destroy(FindObjectOfType<NetworkManager>().gameObject);
		NetworkManager.Shutdown();
		Shutdown();
		SceneManager.LoadScene("MainMenuScene");
	}

	private IEnumerator StartMatchmaking() {
		yield return new WaitUntil(() => FindObjectOfType<NetworkManager>() != null);
		DatabaseManager databaseManager = FindObjectOfType<DatabaseManager>();

		databaseManager.FindLobby(ConnectToLobby);
	}

	private IEnumerator TryToConnectToLobby(Lobby lobby) {
		// TODO: Check if lobby is null, because it means all servers are full
		const String TAG = "TryToConnectToLobby()";
		// TODO: Maybe add a progress spinner or message or something
		NetworkManager networkManager = FindObjectOfType<NetworkManager>();
		TelepathyTransport telepathyTransport = FindObjectOfType<TelepathyTransport>();
		if (currentRetriesCount < maxRetriesCount - 1) {
			if (lobby != null) {
				networkManager.networkAddress = lobby.IpAddress;
				telepathyTransport.port = UInt16.Parse(lobby.Port);
				networkManager.StartClient();
			}

			yield return new WaitForSeconds(retriesInterval);
			if (NetworkClient.isConnected) {
				yield break;
			}

			FindObjectOfType<DatabaseManager>().FreeLobbySlot(lobby);

			networkManager.StopClient();
			currentRetriesCount++;
			Debug.LogWarning($"{TAG}: Retry {currentRetriesCount}/{maxRetriesCount} failed.");
			StartCoroutine(StartMatchmaking());
		} else {
			// If we're here, it always failed, return to main screen
			Debug.LogWarning($"Retries limit ({maxRetriesCount}) reached, returning to Main Menu.");
			ReturnToMainMenu();
		}
	}

	private void ConnectToLobby(Lobby lobby) {
//		FirebaseUser currentFirebaseUser = FindObjectOfType<LoginManager>().GetCurrentUser();
//		NetworkManager networkManager = FindObjectOfType<NetworkManager>();
		StartCoroutine(TryToConnectToLobby(lobby));

		// TODO: Check if enough?
	}

	public void Shutdown() {
		if (_instance == null) {
			Debug.LogWarning("No instances to shutdown!");
			return;
		}

//		FindObjectOfType<DatabaseManager>().RemoveLobby(); // This is now managed on the dedicated server
		Destroy(_instance);
		_instance = null;
	}
}