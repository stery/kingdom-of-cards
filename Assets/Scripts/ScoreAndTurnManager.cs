﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DataModels;
using Firebase.Auth;
using Mirror;
using TMPro;
using UnityEngine;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;
using Random = UnityEngine.Random;


[Serializable]
public struct Score {
	public String PlayerId;
	public String PlayerName;
	public int Attack;
	public int Defense;
	public int RoundsWon;

	public override string ToString() {
		return JsonUtility.ToJson(this);
	}

	public override bool Equals(object obj) {
		if (!(obj is Score)) {
			return false;
		}

		Score objScore = (Score) obj;
		return PlayerId == objScore.PlayerId && Attack == objScore.Attack && Defense == objScore.Defense;
	}
}

public class PlayersScoresList : SyncList<Score> {
}

public class ScoreAndTurnManager : NetworkBehaviour {
	public GameObject scoreAndTurnUi;
	public TextMeshProUGUI enemyScore, myScore;
	public Button endTurnButton;
	public Image turnLine;
	public float turnTime; // Seconds
	private float _timeLeft; // Make sure to sync it, without sync var, if possible

	public Color victoryColor, defeatColor;
	public GameObject endScreen;
	private TextMeshProUGUI _endMessage;
	private Button _returnButton;

	[SyncVar]
	public String currentPlayer;

	private List<String> _playersIds, _playerNames;
	private Score[] _playerScores;

	public int turnsPerRound = 2;
	[SyncVar]
	private int _turnsPlayed;

	public int roundsPerGame = 3;
	private int _roundsPlayed;

	private HashSet<String> _initialisedPlayers;

	[SyncVar]
	private int _initialisedPlayersCount;

	private void Awake() {
		_playersIds = new List<String>(2);
		_playerNames = new List<String>(2);
		_playerScores = new Score[2];
		_initialisedPlayers = new HashSet<String>();
	}

	private void Start() {
		if (isClientOnly) {
			Debug.Log("ScoreAndTurnManager::Start()");
			// Client code
			StartCoroutine(AwaitStartingPlayerToBePicked());
			endTurnButton.onClick.AddListener(EndTurnHandler);
//			scoreAndTurnUi.SetActive(false);
			_endMessage = endScreen.GetComponentInChildren<TextMeshProUGUI>(true);
			_returnButton = endScreen.GetComponentInChildren<Button>(true);
//			endScreen.SetActive(false);
			_returnButton.onClick.AddListener(ReturnButtonHandler);

		} else if (isServerOnly) {
			// Server code
			StartCoroutine(AwaitPlayersToConnect());
		}
	}

	private void Update() {
		//const String TAG = "Update()";
		if (_initialisedPlayersCount == 2) {
			if (isClientOnly) {
				UpdateUi();
			}

			UpdateTimer();
		} else {
			_turnsPlayed = 0;
			_timeLeft = turnTime;
		}
	}

	[Server]
	private IEnumerator AwaitPlayersToConnect() {
		const String TAG = "AwaitPlayersToConnect()";
		Debug.Log($"{TAG}: Waiting for players to connect");
		FindObjectOfType<HeadlessServerManager>().RegisterServerInDatabase();
		yield return new WaitUntil(() => _playersIds != null && _playersIds.Count == 2);
		Debug.Log($"{TAG}: Players connected, picking starting player and initialising scores");
		PickStartingPlayerAndInitialiseScores();
	}

	[Server]
	private void PickStartingPlayerAndInitialiseScores() {
		const String TAG = "PickStartingPlayerAndInitialiseScores()";
		currentPlayer = _playersIds[Random.Range(0, _playersIds.Count - 1)];
		Debug.Log($"{TAG}: Starting player: {currentPlayer}");
		List<Score> playersScoresList = new List<Score>(2);
		_playersIds.ForEach(playerId => {
			int index = _playersIds.IndexOf(playerId);
			Score score = new Score
				{PlayerId = playerId, PlayerName = _playerNames[index], Attack = 0, Defense = 0, RoundsWon = 0};
			Debug.Log($"{TAG}: Trying to add {JsonUtility.ToJson(score)} to scores list");
			playersScoresList.Add(score);
			Debug.Log($"{TAG}: Scores list should now be {JsonUtility.ToJson(playersScoresList)}");
		});

		_playerScores = playersScoresList.ToArray();
		Debug.Log($"{TAG}: playersScoresList size: {playersScoresList.Count}; {JsonUtility.ToJson(playersScoresList)}");
		RpcUpdateScores(_playerScores);
	}

	[Client]
	private IEnumerator AwaitStartingPlayerToBePicked() {
		yield return new WaitUntil(() => !string.IsNullOrEmpty(currentPlayer));

		// TODO
		_timeLeft = turnTime;
		scoreAndTurnUi.SetActive(true);
	}

	[Server]
	public void NotifyPlayerInitialised(String playerId) {
		const String TAG = "NotifyPlayerInitialised()";
		Debug.Log($"{TAG}: Notified to add {playerId}, current set is: {_initialisedPlayers.ToList()} ({_initialisedPlayers.Count})");
		_initialisedPlayers.Add(playerId);
		_initialisedPlayersCount = _initialisedPlayers.Count;
		Debug.Log($"{TAG}: New set is {_initialisedPlayers.ToList()} ({_initialisedPlayers.Count})");
	}

	[Server]
	public void NotifyPlayedCard(String playerId, Card cardStats) {
		const String TAG = "NotifyPlayedCard()";
		Debug.Log($"{TAG}: Notified from {playerId} that {cardStats} was played");
		List<Score> playersScoresList = _playerScores.ToList();
		List<Score> newPlayersScoresList = new List<Score>(2);
		Debug.Log($"{TAG}: scores list before: {ScoresListToString(playersScoresList)}");
		playersScoresList.ForEach(score => {
			if (score.PlayerId == playerId) {
				score.Attack += cardStats.Attack;
				score.Defense += cardStats.Defense;
				Debug.Log($"{TAG}: TRIED TO UPDATE SCORE {JsonUtility.ToJson(score)}");
			}

			newPlayersScoresList.Add(score);
		});


		_playerScores = newPlayersScoresList.ToArray();
		Debug.Log(
			$"{TAG}: scores list after: {ScoresListToString(playersScoresList)}; {ScoresListToString(newPlayersScoresList)}");
		Debug.Log($"{TAG}: Calling rpc to update score texts");
		RpcUpdateScores(_playerScores);
	}

	[ClientRpc]
	private void RpcUpdateScores(Score[] playersScores) {
		const String TAG = "RpcUpdateScoreTexts()";
		FirebaseUser currentFirebaseUser = FindObjectOfType<LoginManager>().GetCurrentUser();
		Debug.Log($"{TAG}: Trying to update texts");
		_playerScores = playersScores;
		playersScores.ToList().ForEach(score => {
			TextMeshProUGUI textToEdit = score.PlayerId == currentFirebaseUser.UserId ? myScore : enemyScore;
			textToEdit.text = $"{score.PlayerName}\n{score.Attack} atk, {score.Defense} def\nW: {score.RoundsWon}";
			textToEdit.text += $"\nTurns: {_turnsPlayed}/{turnsPerRound}"; // TODO: Remove, debug purpose
			Debug.Log($"{TAG}: Text updated to '{textToEdit.text}'");
		});
	}

	[Server]
	public void RegisterPlayer(String playerId, String playerName) {
		Debug.Log($"RegisterPlayer(): Adding {playerName} ({playerId}) to playersIds list");
		_playersIds.Add(playerId);
		_playerNames.Add(playerName);
	}

	private String ScoresListToString(List<Score> scoresList) {
		String result = "";
		scoresList.ForEach(score => result += " " + score);
		return result;
	}

	[Client]
	private void UpdateUi() {
		PlayerController localPlayer = GetLocalPlayer();
		// TODO: Update line and other stuff
		if (currentPlayer == localPlayer.PlayerId) {
			endTurnButton.interactable = true;
		} else {
			endTurnButton.interactable = false;
		}
	}

	[Client]
	private PlayerController GetLocalPlayer() {
		PlayerController localPlayer = FindObjectsOfType<PlayerController>()
			.ToList()
			.Find(player => player.isLocalPlayer);
		return localPlayer;
	}

	private void UpdateTimer() {
		_timeLeft -= Time.deltaTime;
		_timeLeft = Mathf.Max(0, _timeLeft);
		if (isClientOnly) {
			turnLine.fillAmount = _timeLeft / turnTime;
		}  else if (isServerOnly) {
			if (_timeLeft <= 0) {
				EndTurn();
			}
		}
	}

	[Client]
	private void EndTurnHandler() {
		PlayerController localPlayer = GetLocalPlayer();
		localPlayer.CmdEndTurn();
	}

	[ClientRpc]
	private void RpcResetTimer() {
		_timeLeft = turnTime;
	}

	[Server]
	public void EndTurn() {
		const String TAG = "EndTurn()";
		Debug.Log($"{TAG}: Turn {_turnsPlayed + 1} ended");
		currentPlayer = _playersIds.Find(id => id != currentPlayer);
		// TODO: Update scores/rounds
		_turnsPlayed++;
		if (_turnsPlayed == turnsPerRound) {
			// TODO: End round
			EndRound();
		}

		RpcResetTimer();
		RpcUpdateScores(_playerScores);
		_timeLeft = turnTime;
	}

	[Server]
	private void EndRound() {
		const String TAG = "EndRound()";
		_turnsPlayed = 0;
		List<Score> newScores = new List<Score>(2);
		Score bestScore = new Score();
		_playerScores.ToList()
			.ForEach(score => {
				if (string.IsNullOrEmpty(bestScore.PlayerId) ||
				    score.Attack + score.Defense > bestScore.Attack + bestScore.Defense) {
					bestScore = score;
				}
			});
		Debug.Log($"{TAG}: Best score is {bestScore}");
		_playerScores.ToList()
			.ForEach(score => {
				if (score.Equals(bestScore)) {
					score.RoundsWon++;
				}

				score.Attack = 0;
				score.Defense = 0;
				newScores.Add(score);
			});
		_playerScores = newScores.ToArray();
		RpcUpdateScores(_playerScores);
		_roundsPlayed++;
		if (_roundsPlayed == roundsPerGame) {
			EndGame();
		}
	}

	[Server]
	private void EndGame() {
		Boolean draw = _playerScores[0].RoundsWon == _playerScores[1].RoundsWon;
		Score bestScore = new Score();
		_playerScores.ToList()
			.ForEach(score => {
				if (string.IsNullOrEmpty(score.PlayerId) || score.RoundsWon > bestScore.RoundsWon) {
					bestScore = score;
				}
			});
		RpcEndGame(draw, bestScore.PlayerId);
		FindObjectOfType<HeadlessServerManager>().DoCleanup();
		// TODO: Check if enough
	}

	[ClientRpc]
	private void RpcEndGame(Boolean draw, String winnerId) {
		endScreen.SetActive(true);
		if (draw) {
			_endMessage.text = "Draw";
		} else if (winnerId == GetLocalPlayer().PlayerId) {
			_endMessage.text = "Victory";
			_endMessage.color = victoryColor;
		} else {
			_endMessage.text = "Defeat";
			_endMessage.color = defeatColor;
		}
		FindObjectOfType<NetworkManager>().StopClient();
	}

	//[Client]
	private void ReturnButtonHandler() {
		MatchmakingManager.GetInstance().ReturnToMainMenu();
		// TODO: Check if anything else to do
	}
}