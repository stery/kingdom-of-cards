using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataModels;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Extensions;
using UnityEngine;

public class DatabaseManager : MonoBehaviour {
	private LoginManager _loginManager;
	private FirebaseDatabase _database;
	private MainMenuManager _mainMenuManager;
	private User _currentUser;
	private Boolean _initialised;

	private static DatabaseManager _instance;

	public User CurrentUser => _currentUser;

	private void Awake() {
		if (_instance) {
			DestroyImmediate(gameObject);
		} else {
			DontDestroyOnLoad(gameObject);
			_instance = this;
		}
	}

	private void Start() {
		Debug.Log("DatabaseManager Start()"); // Checking behaviour when scene changes
		_loginManager = FindObjectOfType<LoginManager>();
		_mainMenuManager = FindObjectOfType<MainMenuManager>(); // TODO: Check if this runs when scene changes
		StartCoroutine(AwaitFirebaseAppInitAndGetDatabaseReference());
	}

	private IEnumerator AwaitFirebaseAppInitAndGetDatabaseReference() {
		yield return new WaitUntil(_loginManager.GetInitialisedStatus);
		Debug.Log("Getting database instance, waiting for user to login");
		_database = FirebaseDatabase.DefaultInstance;
		yield return new WaitUntil(() => _loginManager.GetCurrentUser() != null);
		Debug.Log("User logged in");
		FirebaseUser currentFirebaseUser = _loginManager.GetCurrentUser();

		Task.Run(() => GetUserFromDatabase(currentFirebaseUser)).ContinueWith(task => {
			if (!task.IsCompleted) {
				Debug.LogWarning($"An error occurred with the GetUserFromDatabase: {task.Exception}");
				return false;
			}

			Debug.Log($"After all, userjson is {JsonUtility.ToJson(_currentUser)}");
			if (_currentUser == null) {
				Debug.LogWarning(
					"Something went wrong, the user after GET is null, or maybe he just doesn't exist in DB");
//			yield break;
				_currentUser = User.CreateStarterUser($"New Player {currentFirebaseUser.UserId.Substring(0, 6)}");
				WriteCurrentUserToDatabase();
			}

			RegisterUserUpdatesHandler();

			_initialised = true;
			return true;
		}).ContinueWithOnMainThread(task => {
			if (!task.IsCompleted || task.Result == false) {
				Debug.LogWarning($"Could not continue on main thread: {task.Exception}");
				return;
			}

			if (_mainMenuManager != null) {
				Debug.Log("Displaying main menu");
				_mainMenuManager.DisplayMainMenu();
			}
		});

		Debug.Log($"Local IP: {IpFinder.GetPublicIp()}");
		// TODO: Continue
	}

	private void GetUserFromDatabase(FirebaseUser firebaseUser) {
		String userId = firebaseUser.UserId;
		_database.GetReference($"users/{userId}")
			.GetValueAsync()
			.ContinueWith(task => {
				if (task.IsFaulted) {
					Debug.LogError($"An error occurred on the users/{userId} GET: {task.Exception}");
					return;
				}

				if (task.IsCompleted) {
					DataSnapshot dataSnapshot = task.Result;
					String jsonSnapshot = dataSnapshot.GetRawJsonValue();
					Debug.Log($"users/{userId}: {jsonSnapshot}");
					_currentUser = JsonUtility.FromJson<User>(jsonSnapshot);
				}
			}).Wait();
		// TODO
	}

	private void WriteCurrentUserToDatabase() {
		FirebaseUser currentFirebaseUser = _loginManager.GetCurrentUser();
		if (currentFirebaseUser == null) {
			Debug.LogWarning("No firebase users found in the login manager");
			return;
		}

		String userId = currentFirebaseUser.UserId;
		String userJson = JsonUtility.ToJson(_currentUser);
		Debug.Log($"userJson before POST: {userJson}");
		_database.GetReference("users")
			.Child(userId)
			.SetRawJsonValueAsync(userJson)
			.ContinueWithOnMainThread(task => {
				if (task.IsFaulted) {
					Debug.LogWarning($"Failed to write {userJson} to users/{userId}");
					return;
				}

				if (task.IsCompleted) {
					Debug.Log($"Write successful for users/{userId}");
					return;
				}
			});
		// TODO?
	}

	public void GetLocationRewardsFromDatabase(Action<List<LocationReward>> locationRewardsCallback) {
		if (!_initialised) {
			Debug.LogWarning("GetLocationRewards...(): Database not yet initialised!");
			return;
		}

		_database.GetReference("locations")
			.GetValueAsync()
			.ContinueWith(task => {
				if (!task.IsCompleted) {
					Debug.LogWarning($"An error occurred while trying to retrieve locations: {task.Exception}");
					return null;
				}

				DataSnapshot dataSnapshot = task.Result;
				String wrappedJsonSnapshot = $"{{\"list\":{dataSnapshot.GetRawJsonValue()}}}";
				Debug.Log($"locationRewards response: {wrappedJsonSnapshot}");
				return JsonUtility.FromJson<ListWrapper<LocationReward>>(wrappedJsonSnapshot).list;
			}).ContinueWithOnMainThread(task => {
				Debug.Log($"Back on main thread, {task.Status} and {task.Exception}");
				Debug.Log($"with task result: {task.Result}");
				if (!task.IsCompleted || task.Result == null) {
					Debug.LogWarning(
						$"An error occurred while trying to continue on main thread with locations: {task.Exception}");
					return;
				}

				Debug.Log($"Calling locationRewardsCallback with {task.Result}");
				locationRewardsCallback(task.Result);
			});
	}

	public void AddCardToUserCollection(Card card, Action actionCallback = null) {
		const String TAG = "AddCardToUserCollection()";
		if (!_initialised) {
			Debug.LogWarning($"{TAG}: Database not yet initialised!");
			return;
		}

		FirebaseUser currentFirebaseUser = _loginManager.GetCurrentUser();
		if (currentFirebaseUser == null) {
			Debug.LogWarning($"{TAG}: firebaseuser is null, check loginManager");
			return;
		}

		_database.GetReference("users/")
			.Child(currentFirebaseUser.UserId)
			.RunTransaction(mutableData => {
				Debug.Log($"{TAG}: transaction started");
				Dictionary<string, object> user = mutableData.Value as Dictionary<string, object>;
				if (user == null) {
					Debug.Log($"{TAG}: user is null after GET users/{currentFirebaseUser.UserId}");
					user = _currentUser.ToSimpleDictionary();
				}

				List<object> cardsCollection = user["CardsCollection"] as List<object>;
				if (cardsCollection == null) {
					Debug.Log($"{TAG}: cards collection is null");
					return TransactionResult.Abort();
				}

				cardsCollection.Add(card.ToSimpleDictionary());
				//_currentUser.CardsCollection.Add(card); // This will be handled by the DB user changes handler
				Debug.Log($"Added card {card.CardName} to user {currentFirebaseUser.UserId}");

				mutableData.Value = user;
				return TransactionResult.Success(mutableData);
			}).ContinueWithOnMainThread(task => {
				Debug.Log($"{TAG}: Back on main thread after transaction");
				if (!task.IsCompleted) {
					Debug.LogWarning($"{TAG}: Transaction failed");
					return;
				}

				actionCallback?.Invoke();
			});
	}

	public void RemoveLocationReward(LocationReward locationReward, Action actionCallback = null) {
		//TODO: Remove
		const String TAG = "RemoveLocationReward()";
		if (!_initialised) {
			Debug.LogWarning($"{TAG}: Database not yet initialised!");
			return;
		}

		_database.GetReference("locations/")
			.RunTransaction(mutableData => {
				List<object> locationRewards = mutableData.Value as List<object>;
				if (locationRewards == null) {
					Debug.LogWarning($"{TAG}: locationRewards is null after GET locations/");
					return TransactionResult.Abort();
				}

				object reference = null;

				foreach (var reward in locationRewards) {
					if (!(reward is Dictionary<string, object>)) {
						continue;
					}

					Dictionary<string, object> dictionaryReward = (Dictionary<string, object>) reward;
					double lat = (double) dictionaryReward["Latitude"];
					double lon = (double) dictionaryReward["Longitude"];
					if (Math.Abs(lat - locationReward.Latitude) < 0.00001 &&
					    Math.Abs(lon - locationReward.Longitude) < 0.00001) {
						reference = reward;
						break;
					}
				}

				if (reference != null) {
					locationRewards.Remove(reference);
					Debug.Log($"Removed location ({locationReward.Latitude}, {locationReward.Longitude})");
				} else {
					Debug.LogWarning(
						$"Location ({locationReward.Latitude}, {locationReward.Longitude}) not found in DB!");
					return TransactionResult.Abort();
				}

				mutableData.Value = locationRewards;
				return TransactionResult.Success(mutableData);
			}).ContinueWithOnMainThread(task => {
				if (!task.IsCompleted) {
					Debug.LogWarning($"{TAG}: Transaction failed: {task.Exception}");
					return;
				}

				actionCallback?.Invoke();
			});
	}

	public void AttachChildAddedHandler(String referencePath, EventHandler<ChildChangedEventArgs> handler) {
		_database.GetReference(referencePath).ChildAdded += handler;
	}

	public void DetachChildAddedHandler(String referencePath, EventHandler<ChildChangedEventArgs> handler) {
		_database.GetReference(referencePath).ChildAdded -= handler;
	}

	public void AttachChildRemovedHandler(String referencePath, EventHandler<ChildChangedEventArgs> handler) {
		_database.GetReference(referencePath).ChildRemoved += handler;
	}

	public void DettachChildRemovedHandler(String referencePath, EventHandler<ChildChangedEventArgs> handler) {
		_database.GetReference(referencePath).ChildRemoved -= handler;
	}

	public void AttachValueChangedHandler(String referencePath, EventHandler<ValueChangedEventArgs> handler) {
		_database.GetReference(referencePath).ValueChanged += handler;
	}

	public void DettachValueChangedHandler(String referencePath, EventHandler<ValueChangedEventArgs> handler) {
		_database.GetReference(referencePath).ValueChanged -= handler;
	}

	public void RegisterUserUpdatesHandler() {
		FirebaseUser firebaseUser = _loginManager.GetCurrentUser();
		_database.GetReference("users").Child(firebaseUser.UserId).ValueChanged += UserUpdatesHandler;
	}

	public void UnregisterUserUpdatesHandler() {
		FirebaseUser firebaseUser = _loginManager.GetCurrentUser();
		_database.GetReference("users").Child(firebaseUser.UserId).ValueChanged -= UserUpdatesHandler;
	}

	private void UserUpdatesHandler(object source, ValueChangedEventArgs args) {
		const String TAG = "UserUpdatesHandler()";
		if (args.DatabaseError != null) {
			Debug.LogWarning($"{TAG}: database errors: {args.DatabaseError.Message}");
			return;
		}

		User newUser = JsonUtility.FromJson<User>(args.Snapshot.GetRawJsonValue());
		Debug.Log($"{TAG}: user changes: {JsonUtility.ToJson(newUser)}; " +
		          $"current user(before update): {JsonUtility.ToJson(_currentUser)}");
		// TODO: Update currentUser
		_currentUser = newUser;
	}

	public void FindLobby(Action<Lobby> callback = null) {
		const String TAG = "FindLobby()";
		if (!_initialised) {
			Debug.LogWarning($"{TAG}: Database not yet initialised!");
			return;
		}

		Lobby lobbyToConnect = null;
		_database.GetReference("lobbies")
			.RunTransaction(mutableData => {
				List<object> lobbiesList = mutableData.Value as List<object>;
				lobbyToConnect = null;
				if (lobbiesList == null) {
					Debug.Log($"{TAG}: lobbies list is null (or DB is out of date)");
					lobbiesList = new List<object>();
				}

				foreach (var lobbyObject in lobbiesList) {
					if (!(lobbyObject is Dictionary<string, object>)) {
						continue;
					}

					Dictionary<string, object> lobbyDictionary = (Dictionary<string, object>) lobbyObject;
					Lobby currentLobby;
					try {
						currentLobby = Lobby.FromSimpleDictionary(lobbyDictionary);
					} catch (Exception e) {
						Debug.LogWarning($"{TAG}: Could not convert to lobby object: {e.Message}");
						return TransactionResult.Abort();
					}

					if (currentLobby == null) {
						continue;
					}

					int count = int.Parse(currentLobby.ConnectedPlayers);

					if (count < 2) {
						count++; // Mark it as occupied, it will be freed if could not connect
						currentLobby.ConnectedPlayers = count.ToString();
						lobbyToConnect = currentLobby;
						currentLobby.CopyLobbyToDictionary(lobbyDictionary);
						Debug.Log($"{TAG}: Found a free lobby: {currentLobby}");
						break;
					}
				}

				mutableData.Value = lobbiesList;
				return TransactionResult.Success(mutableData);
			}).ContinueWithOnMainThread(task => {
				if (!task.IsCompleted) {
					Debug.LogWarning($"{TAG}: Lobby transaction task failed!");
					return;
				}

				if (lobbyToConnect == null) {
					Debug.Log($"{TAG}: LobbyToConnect is null, either all lobbies are full, or the db is out of date");
				}

				// TODO: Notify matchmaking manager about lobby;
				callback?.Invoke(lobbyToConnect);
			});
	}

	public void FreeLobbySlot(Lobby lobby) {
		const String TAG = "FreeLobbySlot()";

		if (!_initialised) {
			Debug.LogWarning($"{TAG}: Database not yet initialised!");
			return;
		}

		if (lobby == null) {
			Debug.LogWarning($"{TAG}: Target lobby is null, aborting!");
			return;
		}

		_database.GetReference("lobbies")
			.RunTransaction(mutableData => {
				List<object> lobbiesList = mutableData.Value as List<object>;
				if (lobbiesList == null) {
					Debug.Log($"{TAG}: lobbies list is null (or DB is out of date)");
					lobbiesList = new List<object>();
				}

				foreach (var lobbyObject in lobbiesList) {
					if (!(lobbyObject is Dictionary<string, object>)) {
						continue;
					}

					Dictionary<string, object> lobbyDictionary = (Dictionary<string, object>) lobbyObject;
					Lobby currentLobby;
					try {
						currentLobby = Lobby.FromSimpleDictionary(lobbyDictionary);
					} catch (Exception e) {
						Debug.LogWarning($"{TAG}: Could not convert to lobby object: {e.Message}");
						return TransactionResult.Abort();
					}

					if (currentLobby.IpAddress == lobby.IpAddress && currentLobby.Port == lobby.Port) {
						int count = int.Parse(lobby.ConnectedPlayers);
						// This is the one
						/*currentLobby.Available = false;
						lobbyDictionary["Available"] =
							false; // Feeling a bit unsafe about this, about being the same reference as lobbyObject*/
						count--;
						lobby.ConnectedPlayers = count.ToString();
						lobby.CopyLobbyToDictionary(lobbyDictionary);
						break;
					} // TODO: Check behaviour if not found
				}

				mutableData.Value = lobbiesList;
				return TransactionResult.Success(mutableData);
			});
	}
}