﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataModels;
using Firebase;
using Firebase.Database;
using Firebase.Extensions;
using Firebase.Unity.Editor;
using Mirror;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HeadlessServerManager : NetworkBehaviour {
	private TelepathyTransport _telepathyTransport;
	private ushort _listenPort;
	private string _ipAddress;
	private FirebaseDatabase _database;
	private Boolean _initialisedDb;
	private Lobby _myLobby;

	[SerializeField]
	private Boolean DEBUG;

	private void Awake() {
		if (!NetworkManager.isHeadless && !DEBUG) {
			Debug.Log($"Server is not headless, destroying {name}");
			Destroy(gameObject);
			return;
		}

		_telepathyTransport = FindObjectOfType<TelepathyTransport>();
		String portArgValue = GetArg("--port") ?? GetArg("-p");
		if (portArgValue == null && !DEBUG) {
			Debug.Log("Port missing, shutting down");
			Application.Quit();
			return; // Just to assure Rider the rest of the code doesn't execute
		}

		// TODO: Init DB
		InitialiseDatabase();

		_listenPort = ushort.Parse(DEBUG ? "7777" : portArgValue);
		try {
			_ipAddress = IpFinder.GetPublicIp();
		} catch (Exception e) {
			Debug.LogWarning($"HeadlessServerManager failed to retrieve IP Address ({e.Message}), shutting down");
			Application.Quit();
			return;
		}

		// TODO: Wait for the server startup to complete in case it was restarted
		SetListenPortAndStartServer();
	}

	private void OnApplicationQuit() {
		Debug.Log("APPLICATION QUIT");
		if (_myLobby != null) {
			UnregisterServerFromDatabase(null); // Will it make the call?
		}
	}

	private void InitialiseDatabase() {
		FirebaseApp app = FirebaseApp.DefaultInstance;
		app.SetEditorDatabaseUrl("https://kingdom-of-cards-1559820426626.firebaseio.com/");
		_database = FirebaseDatabase.DefaultInstance;
		_initialisedDb = true;
	}

	private void SetListenPortAndStartServer() {
		_telepathyTransport.port = _listenPort;
		NetworkManager networkManager = FindObjectOfType<NetworkManager>();
		if (!networkManager.StartServer()) {
			Debug.Log("Server failed to start, shutting down");
			Application.Quit();
			return;
		}
		
//		RegisterServerInDatabase();
	}

	public void RegisterServerInDatabase() {
		_myLobby = new Lobby(_ipAddress, _listenPort.ToString(), "0");
		CreateLobby(_myLobby);
	}

	private void UnregisterServerFromDatabase(Action callback) {
		RemoveLobby(_myLobby, callback);
	}

	private String GetArg(String searchedArg) {
		List<String> args = Environment.GetCommandLineArgs().ToList();
		foreach (string arg in args) {
			if (arg.Equals(searchedArg)) {
				int index = args.IndexOf(arg);
				if (index + 1 < args.Count) {
					return args[index + 1];
				} else {
					Debug.Log("Argument value not passed, probably");
					return null;
				}
			}
		}

		return null;
	}

	public void DoCleanup() {
//		ResetLobby(_myLobby);
		UnregisterServerFromDatabase(() => {
			NetworkManager networkManager = FindObjectOfType<NetworkManager>();
			networkManager.StopServer();
			NetworkManager.Shutdown();
			Destroy(networkManager);
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			// TODO: Find if should do anything else
		});
	}

	public void CreateLobby(Lobby lobby) {
		const String TAG = "CreateLobby()";
		if (!_initialisedDb) {
			Debug.LogWarning($"{TAG}: Database not yet initialised!");
			return;
		}

		_database.GetReference("lobbies")
			.RunTransaction(mutableData => {
				List<object> lobbiesList = mutableData.Value as List<object>;
				if (lobbiesList == null) {
					Debug.Log($"{TAG}: lobbies list is null (or DB is out of date)");
					lobbiesList = new List<object>();
					// No lobbies, so either data is old or really there are no lobbies, add one
				}

				foreach (var lobbyObject in lobbiesList) {
					if (!(lobbyObject is Dictionary<string, object>)) {
						continue;
					}

					Dictionary<string, object> lobbyDictionary = (Dictionary<string, object>) lobbyObject;
					Lobby currentLobby;
					try {
						currentLobby = Lobby.FromSimpleDictionary(lobbyDictionary);
					} catch (Exception e) {
						Debug.LogWarning($"{TAG}: Could not convert to lobby object: {e.Message}");
						return TransactionResult.Abort();
					}

					if (currentLobby.IpAddress == lobby.IpAddress && currentLobby.Port == lobby.Port) {
						Debug.LogWarning($"{TAG}: Found a lobby with the same IP and port: {currentLobby}");
						return TransactionResult.Abort();
					}
				}

				object newLobbyObject = new Dictionary<string, object>
				{
					["IpAddress"] = lobby.IpAddress,
					["Port"] = lobby.Port,
					["ConnectedPlayers"] = lobby.ConnectedPlayers
				};

//				newLobbyObject = (object) new Dictionary<string, object>(lobby.ToSimpleDictionary());

				lobbiesList.Add(newLobbyObject);
				mutableData.Value = lobbiesList;
				Debug.Log($"{TAG}: Added lobby {lobby} to lobbies list");
				return TransactionResult.Success(mutableData);
			}).ContinueWithOnMainThread(task => {
				if (!task.IsCompleted) {
					Debug.LogWarning($"{TAG}: {task.Exception}");
				}
			});
	}

	public void ResetLobby(Lobby lobby) {
		const String TAG = "ResetLobby()";
		if (!_initialisedDb) {
			Debug.LogWarning($"{TAG}: Database not yet initialised!");
			return;
		}

		_database.GetReference("lobbies")
			.RunTransaction(mutableData => {
				List<object> lobbiesList = mutableData.Value as List<object>;
				Boolean shouldCreate = false;
				if (lobbiesList == null) {
					Debug.Log($"{TAG}: lobbies list is null (or DB is out of date)");
					lobbiesList = new List<object>();
					shouldCreate = true;
					// No lobbies, so either data is old or really there are no lobbies, add one
				}

				foreach (var lobbyObject in lobbiesList) {
					if (!(lobbyObject is Dictionary<string, object>)) {
						continue;
					}

					Dictionary<string, object> lobbyDictionary = (Dictionary<string, object>) lobbyObject;
					Lobby currentLobby;
					try {
						currentLobby = Lobby.FromSimpleDictionary(lobbyDictionary);
					} catch (Exception e) {
						Debug.LogWarning($"{TAG}: Could not convert to lobby object: {e.Message}");
						return TransactionResult.Abort();
					}

					if (currentLobby.IpAddress == lobby.IpAddress && currentLobby.Port == lobby.Port) {
						// Found our lobby
						shouldCreate = false;
						lobby.ConnectedPlayers = "0";
						lobbyDictionary["ConnectedPlayers"] = "0";
						Debug.Log($"{TAG}: Found requested lobby, updated to {lobby}");
						break;
					}
				}

				if (shouldCreate) {
					lobbiesList.Add(lobby.ToSimpleDictionary());
					Debug.Log($"{TAG}: Lobby not found, added it ({lobby})");
				}

				mutableData.Value = lobbiesList;
				return TransactionResult.Success(mutableData);
			});
	}

	public void RemoveLobby(Lobby lobby, Action callback) {
		const String TAG = "RemoveLobby()";
		if (!_initialisedDb) {
			Debug.LogWarning($"{TAG}: Database not yet initialised!");
			return;
		}

		Task transactionTask = _database.GetReference("lobbies")
			.RunTransaction(mutableData => {
				List<object> lobbiesList = mutableData.Value as List<object>;
				if (lobbiesList == null) {
					Debug.Log($"{TAG}: lobbies list is null, or db is out of date");
					/*lobbiesList = new List<object>(1);
					mutableData.Value = lobbiesList;*/
					return TransactionResult.Success(mutableData);
				}

				object lobbyToRemove = null;

				foreach (var lobbyObject in lobbiesList) {
					if (!(lobbyObject is Dictionary<string, object>)) {
						continue;
					}

					Dictionary<string, object> lobbyDictionary = (Dictionary<string, object>) lobbyObject;
					Lobby currentLobby;
					try {
						currentLobby = Lobby.FromSimpleDictionary(lobbyDictionary);
					} catch (Exception e) {
						Debug.LogWarning($"{TAG}: Could not convert to lobby object: {e.Message}");
						return TransactionResult.Abort();
					}

					if (currentLobby.IpAddress == lobby.IpAddress && currentLobby.Port == lobby.Port) {
						// This is our lobby, remove it
						lobbyToRemove = lobbyObject;
						break;
					}
				}

				if (lobbyToRemove == null) {
					Debug.LogWarning($"{TAG}: Lobby not found");
					return TransactionResult.Abort();
				}

				lobbiesList.Remove(lobbyToRemove);
				mutableData.Value = lobbiesList;
				Debug.Log($"{TAG}: Successfully removed lobby {lobby}");
				return TransactionResult.Success(mutableData);
			}).ContinueWithOnMainThread(task => {
				if (!task.IsCompleted) {
					Debug.Log($"{TAG}: Transaction failed");
				} else {
					Debug.Log($"{TAG}: Transaction successful");
				}
				
				callback?.Invoke();
			})/*.Wait()*/;

		if (callback != null) {
			transactionTask.Wait(10000);
		}
	}
}