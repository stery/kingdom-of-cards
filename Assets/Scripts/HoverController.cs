﻿using System;
using UnityEngine;

public class HoverController : MonoBehaviour {

    private Outline _outline;
    private Animator _animator;

    private Boolean _dragging;
    private GameManagerController _gameManagerController;

    // Start is called before the first frame update
    void Start() {
        _outline = GetComponent<Outline>();
        _outline.enabled = false;
        _animator = GetComponentInChildren<Animator>();
        _gameManagerController = FindObjectOfType<GameManagerController>(); // TODO: Check if should change after implementing networking
        // Above generates error if card spawned before the player, for some reason (the gamemanager gets disabled until game starts)
//        Debug.Log(_gameManagerController.currentlyDraggedCard);
    }
    
    private void OnMouseEnter() {
//        Debug.Log($"Entered {name} with the mouse. GameManagerController: {_gameManagerController}, {_outline}");
        if (_gameManagerController == null) {
            // Could happen due to networking init stuff
            _gameManagerController = FindObjectOfType<GameManagerController>();
        }

        if (_outline == null) {
            _outline = GetComponent<Outline>();
            Debug.Log($"{name} didn't have the outline defined...");
        }
        // Outline this card only when no cards are dragged or if this is the one being dragged
        if (!_gameManagerController.currentlyDraggedCard || _gameManagerController.currentlyDraggedCard == gameObject) {
//            Debug.Log($"{name} should now be outlined");
            _outline.enabled = true;
        }
        
        Debug.Log($"{name} has {_outline} {_outline.enabled}");

        GrowCard();
    }

    private void OnMouseExit() {
        _outline.enabled = false;
        if (!_dragging) {
            ShrinkCard();
        }
    }

    public void GrowCard() {
        // Prevent growth if a card is currently being dragegd
        if (_animator == null) {
            return;
        }
        if (!_gameManagerController.currentlyDraggedCard) {
            _animator.SetBool("shrinked", false);
        }
    }

    public void ShrinkCard() {
        if (_animator == null) {
            return;
        }
        _animator.SetBool("shrinked", true);
    }

    private void OnMouseDrag() {
//        Debug.Log($"Mouse Drag on {name}");
        if (!_dragging) {
            _dragging = true;
        }
    }

    private void OnMouseUp() {
//        Debug.Log($"Mouse no longer dragging {name}");
        _dragging = false;
//        shrinkCard(); // Must be used to prevent issues with the dragging to the board
    }
}
