﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeCircle : MonoBehaviour {
    
    public float radius;
    public int segmentsCount;
    public Color color;
    public float yOffset;
    public Boolean useWorldSpace;

    private LineRenderer _lineRenderer;

    private void Start() {
        _lineRenderer = GetComponent<LineRenderer>();
        if (_lineRenderer) {
            DrawPoints();
        }
    }

    public void DrawPoints() {
        _lineRenderer.material.color = color;
        _lineRenderer.positionCount = segmentsCount + 2;
        _lineRenderer.useWorldSpace = useWorldSpace;

        float angle = 360f / segmentsCount;

        for (int i = 0; i <= segmentsCount + 1; i++) {
            float x = Mathf.Cos(Mathf.Deg2Rad * i * angle) * radius;
            float z = Mathf.Sin(Mathf.Deg2Rad * i * angle) * radius;
            _lineRenderer.SetPosition(i, new Vector3(x, yOffset, z));
        }
    }
}
