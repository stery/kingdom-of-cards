﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataModels;
using Firebase.Database;
using Firebase.Extensions;
using Mapbox.Examples;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MapManager : MonoBehaviour {
	private LoginManager _loginManager;
	private DatabaseManager _databaseManager;

	public GameObject mapCardPrefab;
	public float spawnScale;

	[SerializeField]
	private AbstractMap map;

	private Dictionary<GameObject, Vector2d> _spawnedObjects;
	private List<LocationReward> _queuedForRemoval;
	private List<LocationReward> _queuedForAdding;
	private Boolean _shouldUpdateLocations;
	private List<LocationReward> _updatedLocations;

	private void Awake() {
		_spawnedObjects = new Dictionary<GameObject, Vector2d>();
		_queuedForRemoval = new List<LocationReward>();
		_queuedForAdding = new List<LocationReward>();
	}

	private void Start() {
		_loginManager = FindObjectOfType<LoginManager>();
		_databaseManager = FindObjectOfType<DatabaseManager>();
		
		Debug.Log($"MapManager Start(): current user: {_loginManager.GetCurrentUser().Email}");
		// TODO: Query locations from database and spawn corresponding cards
		StartCoroutine(RetrieveRewards());
	}

	private void Update() {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			// Pressed Back on phone or Escape on PC
			SceneManager.LoadScene("MainMenuScene");
			return;
		}
		
//		_queuedForAdding.ForEach(AddLocationReward);
//		_queuedForAdding.Clear(); // This doesn't feel thread-safe...

		if (_shouldUpdateLocations) {
			SpawnLocationRewards(_updatedLocations);
			_shouldUpdateLocations = false;
		}

		List<GameObject> removedCards = null;

		foreach (KeyValuePair<GameObject,Vector2d> pair in _spawnedObjects) {
			GameObject card = pair.Key;
			Vector2d geoloc = pair.Value;
			
			// Check if the GameObject was destroyed in the meantime
			if (card == null) {
				// Mark it as deleted
				if (removedCards == null) {
					removedCards = new List<GameObject>();
				}
				removedCards.Add(card);
				continue;
			}

			LocationReward match = _queuedForRemoval.Find(reward => Math.Abs(reward.Latitude - geoloc.x) < 0.00001 &&
			                                                        Math.Abs(reward.Longitude - geoloc.y) < 0.00001);

			if (match != null) {
				// There is a reward queued for removal
				Destroy(card); // The next Update loop will handle it
				_queuedForRemoval.Remove(match);
				continue;
			}

			card.transform.localPosition = map.GeoToWorldPosition(geoloc, false);
			card.transform.localScale = Vector3.one * spawnScale;
		}

		if (removedCards != null) {
			removedCards.ForEach(card => _spawnedObjects.Remove(card));
		}
	}

	private IEnumerator RetrieveRewards() {
		// The callback for when the database is ready
//		Action<List<LocationReward>> locationRewardsAction = list => {
//			SpawnLocationRewards(list);
//			SubscribeToLocationRewardsDatabaseChanges();
//		};
//		_databaseManager.GetLocationRewardsFromDatabase(locationRewardsAction);
		SubscribeToLocationRewardsDatabaseChanges();
		yield return null;
	}

	private void SpawnLocationRewards(List<LocationReward> locationRewards) {
		// TODO: Spawn
		Debug.Log($"SpawnLocationRewards was called, locationRewards size: {locationRewards?.Count ?? 0}");

		foreach (KeyValuePair<GameObject,Vector2d> pair in _spawnedObjects) {
			Destroy(pair.Key);
		}
		_spawnedObjects.Clear();
		Debug.Log($"New dictionary size after clear: {_spawnedObjects.Count}");

		if (locationRewards == null) {
			return;
		}

		foreach (LocationReward locationReward in locationRewards) {
			/*Card cardStats = locationReward.Card;
			double latitude = locationReward.Latitude;
			double longitude = locationReward.Longitude;
			
			Debug.Log($"Trying to instantiate {cardStats.CardName} at ({latitude}, {longitude})");
			
			// TODO: Instantiate and place on map
			GameObject cardGameObject = Instantiate(mapCardPrefab);
			cardGameObject.GetComponent<CardStatsController>().cardStats = cardStats;
			cardGameObject.GetComponent<MapCardController>().locationReward = locationReward;
			Vector2d geoloc = new Vector2d(latitude, longitude);
			_spawnedObjects.Add(cardGameObject, geoloc);*/
			AddLocationReward(locationReward);
		}
		
		//SubscribeToLocationRewardsDatabaseChanges(); // TODO: Maybe move this somewhere else, but just maybe.
	}

	private void AddLocationReward(LocationReward locationReward) {
		Card cardStats = locationReward.Card;
		double latitude = locationReward.Latitude;
		double longitude = locationReward.Longitude;
			
		Debug.Log($"Trying to instantiate {cardStats.CardName} at ({latitude}, {longitude})");
			
		// TODO: Instantiate and place on map
		GameObject cardGameObject = Instantiate(mapCardPrefab);
		cardGameObject.GetComponent<CardStatsController>().cardStats = cardStats;
		cardGameObject.GetComponent<MapCardController>().locationReward = locationReward;
		Vector2d geoloc = new Vector2d(latitude, longitude);
		_spawnedObjects.Add(cardGameObject, geoloc);
	}

	private void SubscribeToLocationRewardsDatabaseChanges() {
		// TODO: Define handlers and attach them
//		_databaseManager.AttachChildAddedHandler("locations", LocationAddedHandler);
//		_databaseManager.AttachChildRemovedHandler("locations", LocationRemovedHandler);
		_databaseManager.AttachValueChangedHandler("locations", LocationsChangedHandler);
	}

	private void LocationsChangedHandler(object source, ValueChangedEventArgs args) {
		const String TAG = "LocationsChangedHandler()";
		if (args.DatabaseError != null) {
			Debug.LogWarning($"{TAG}: Database errors: {args.DatabaseError.Message}");
			return;
		}

		String wrappedJsonSnapshot = $"{{\"list\":{args.Snapshot.GetRawJsonValue()}}}";
		List<LocationReward> locationRewards = args.Snapshot.Value != null ? JsonUtility.FromJson<ListWrapper<LocationReward>>(wrappedJsonSnapshot).list : null;
		Debug.Log($"{TAG}: changed locations from db: {JsonUtility.ToJson(locationRewards)} ||| {args.Snapshot.GetRawJsonValue()}");
		_updatedLocations = locationRewards;
		_shouldUpdateLocations = true;
	}

	private void LocationRemovedHandler(object source, ChildChangedEventArgs args) {
		const String TAG = "LocationRemovedHandler()";
		if (args.DatabaseError != null) {
			Debug.LogWarning($"{TAG}: Database errors: {args.DatabaseError.Message}");
			return;
		}

		LocationReward locationReward = JsonUtility.FromJson<LocationReward>(args.Snapshot.GetRawJsonValue());
		Debug.Log($"{TAG}: location removed from db: {args.Snapshot.GetRawJsonValue()}");
		// TODO: remove from spawnedObjects
		_queuedForRemoval.Add(locationReward);
	}

	private void LocationAddedHandler(object source, ChildChangedEventArgs args) {
		const String TAG = "LocationAddedHandler()";
		if (args.DatabaseError != null) {
			Debug.LogWarning($"{TAG}: Database errors: {args.DatabaseError.Message}");
			return;
		}

		LocationReward locationReward = JsonUtility.FromJson<LocationReward>(args.Snapshot.GetRawJsonValue());
		Debug.Log($"{TAG}: new location from db: {locationReward}");
		// Add location
		_queuedForAdding.Add(locationReward);
	}
}
