﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyDeckController : MonoBehaviour
{
    public Color allyCardHoverColor, enemyCardHoverColor;
    private Outline _outline;
    private GameManagerController _gameManagerController;

    private void Start() {
        _outline = GetComponent<Outline>();
        _gameManagerController = FindObjectOfType<GameManagerController>();
        
        if (CompareTag("Player")) {
            _outline.OutlineColor = allyCardHoverColor;
        } else {
            _outline.OutlineColor = enemyCardHoverColor;
        }
    }

    private void OnMouseEnter() {
        Debug.Log($"{name} mouse enter");
        if (!_gameManagerController || (_gameManagerController && !_gameManagerController.currentlyDraggedCard)) {
            _outline.enabled = true;
        }
    }

    private void OnMouseExit() {
        _outline.enabled = false;
    }
}
