﻿using System;
using System.Collections;
using DataModels;
using Mirror;
using TMPro;
using UnityEngine;

public class CardStatsController : MonoBehaviour {
    [SerializeField]
    private int deathTimeout = 3;

    private Color _initialColor;

    public GameObject attackTextGameObject;
    private TextMeshPro _attackText;

    [SerializeField]
    private int initialAttackValue;

    private int _attackValue;
    public GameObject defenseTextGameObject;
    private TextMeshPro _defenseText;

    [SerializeField]
    private int initialDefenseValue;

    private int _defenseValue;

    /*
     * !!! The Card object should be retrieved
     * from the database BEFORE instantiating
     * the card prefab !!!
     */
    public Card cardStats;
    public CardImage cardImage;

    private enum TextType {
        Attack,
        Defense
    }

    /*public override void OnStartClient() {
        base.OnStartClient();
        StartCoroutine(AwaitInitialisation());
    }*/

    // Start is called before the first frame update
    void Start() {
        
        // TODO: Await for the Card object to be initialised
        StartCoroutine(AwaitInitialisation());
    }

    private IEnumerator AwaitInitialisation() {
        //cardStats = null;
        yield return new WaitUntil(IsInitialised);
        
        
        _attackText = attackTextGameObject.GetComponent<TextMeshPro>();
        _defenseText = defenseTextGameObject.GetComponent<TextMeshPro>();
        _initialColor = _attackText.color;

        cardImage = GetComponent<CardImage>();
        
        Debug.Log($"Initializing {name} with cardname = {cardStats.CardName}");
        cardImage.ChangeSpriteWithResourceName(cardStats.CardName);
        _attackValue = initialAttackValue = cardStats.Attack;
        _defenseValue = initialDefenseValue = cardStats.Defense;
        UpdateText(TextType.Attack);
        UpdateText(TextType.Defense);
    }

    public void UpdateAttackValue(int value) {
        _attackValue += Math.Max(_attackValue + value, 99);
        UpdateText(TextType.Attack);
    }

    public void UpdateDefenseValue(int value) {
        _defenseValue = Math.Max(_defenseValue - value, 0);
        UpdateText(TextType.Defense);

        if (_defenseValue == 0) {
            // TODO: Death "animation"
            StartCoroutine(Die());
        }
    }

    private void UpdateText(TextType textType) {
        TextMeshPro textRef = textType == TextType.Attack ? _attackText : _defenseText;
        int valueRef = textType == TextType.Attack ? _attackValue : _defenseValue;
        int initialValueRef = textType == TextType.Attack ? initialAttackValue : initialDefenseValue;

        textRef.SetText(valueRef.ToString());
        if (valueRef < initialValueRef) {
            textRef.color = Color.red;
        } else if (valueRef == initialValueRef) {
            textRef.color = _initialColor;
        } else {
            textRef.color = Color.green;
        }
    }

    private IEnumerator Die() {
        // Sound?
        yield return new WaitForSeconds(deathTimeout);
        Destroy(gameObject);
    }

    public Boolean IsInitialised() {
        return cardStats != null && cardStats.CardName != null && !cardStats.CardName.Equals("");
    }

    public int AttackValue => _attackValue;

    public int DefenseValue => _defenseValue;
}