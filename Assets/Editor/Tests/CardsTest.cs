﻿using System;
using System.Collections;
using DataModels;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Object = UnityEngine.Object;

public class CardsTest {
	[UnityTest]
	public IEnumerator GivenCardStats_CardImageIsSetUsingCardName_WhenCardIsInstantiated() {
		// Arrange
		GameObject cardPrefab = Resources.Load<GameObject>("Prefabs/Card");
		String spriteName = "The Knight";
		Card cardStats = new Card(spriteName, 2, 3);
		Sprite expectedSprite = Resources.Load<Sprite>($"Sprites/{spriteName}");

		// Act
		GameObject card = Object.Instantiate(cardPrefab);
		CardStatsController cardStatsController = card.GetComponent<CardStatsController>();
		CardImage cardImage = card.GetComponent<CardImage>();
		cardStatsController.cardStats = cardStats;
		yield return new WaitUntil(() => cardStatsController.IsInitialised());

		// Assert
		Assert.AreEqual(expectedSprite, cardImage.frontSpriteRenderer.sprite);
	}
}