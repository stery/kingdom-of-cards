﻿Shader "Custom/Stery Outline Shader"
{
    Properties
    {        
        _OutlineWidth ("Outline Width", Range(0, 50.0)) = 0.1
        _OutlineColor ("Outline Color", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        Tags {
            "Queue" = "Transparent+110"
            "RenderType"="Transparent"
            "DisableBatching" = "True"
        }

        Pass
        {
            Stencil {
                Ref 1
                Comp NotEqual
            }
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"

            struct v2f
            {
                float4 vertex : SV_POSITION;
                fixed4 color : COLOR;
            };
            
            float _OutlineWidth;
            float4 _OutlineColor;

            v2f vert (appdata_base v)
            {
                v2f o;
                float3 normal = v.normal;
                float3 viewPosition = UnityObjectToViewPos(v.vertex);
                float3 viewNormal = normalize(mul((float3x3)UNITY_MATRIX_IT_MV, normal));
        
                o.vertex = UnityViewToClipPos(viewPosition + viewNormal * _OutlineWidth);
                o.color = _OutlineColor;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return i.color;
            }
            ENDCG
        }
    }
}
